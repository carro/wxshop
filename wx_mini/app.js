App({
  onLaunch() {

  },
  checkLogin(){
    var that = this;
    if (that.accountInfo() === null){
      wx.showToast({
        title: '请登录',
      });
      wx.navigateTo({
        url: `../login/login`
      })
    }
  },
  rootUrl : 'http://192.168.10.2:8080/',
  //rootUrl : 'http://192.168.0.183:8080/',
  accountInfo: function() {
    var info = wx.getStorageSync('accountInfo') || null;
    return info;
  }
})