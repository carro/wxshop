// pages/user/user.js
const app = getApp();
var that;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    nickname: '请登录',
    visual: 'hidden',
    zong: 0,
    dan: 0,
    motto: '请登录',
    userInfo: app.accountInfo(),
    hasUserInfo: false,
    accountId: null,
    username: '请登录',
    avatarUrl: null,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },

  /**
   * 生命周期函数--监听页面加载
   */
  gotologin: function () {
    app.checkLogin();
  },
  onLoad: function (options) {
      var that =this;
      if (app.accountInfo() !== null) {
        that.setData({
          userInfo: app.accountInfo(),
          hasUserInfo: true
        })
        that.dataItem();
      }
  },
  dataItem: function () {
    var that = this;
    if(app.accountInfo() !== null){
      wx.request({
        url: app.rootUrl + '/api/dataItem',
        data: { 'accountId': app.accountInfo().accountId},
        header: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        method: 'POST',
        success: function (res) {
          that.setData({
            zong: res.data.zong,
            dan: res.data.dan
          });
        }
      })
    }
    
  },
  toOd: function(){
    wx.navigateTo({
      url: '../list/list'
    })
  },
  toSc: function(){
    wx.navigateTo({
      url: '../sc/sc'
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})