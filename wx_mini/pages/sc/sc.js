// pages/list/list.js
const app = getApp()
var that;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    visual: 'hidden',
    cList:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    /*that.setData({
      cList : [{'productId':'1','productTitle':'测试1'},{'productId':'2','productTitle':'测试2'}]
    })*/
  },

  showDetail: function (e) {
    var index = e.currentTarget.dataset.index;
    var that = this;
    if(app.accountInfo() !== null){
      wx.request({
        url: app.rootUrl + '/api/saveCollect',
        data: { 'accountId': app.accountInfo().accountId, 'productId':index },
        header: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        method: 'POST',
        success: function (res) {
          that.onShow();
        }
      })
    }
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    if(app.accountInfo() !== null){
      wx.request({
        url: app.rootUrl + '/api/collectionList',
        data: { 'accountId': app.accountInfo().accountId, 'state': -1 },
        header: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        method: 'POST',
        success: function (res) {
          that.setData({
            cList: res.data.data
          });
        }
      })
    }
    
  },
})