//index.js
//获取应用实例
const app = getApp();
var Bmob = require('../../utils/bmob.js');
var that;
// 最大行数
var max_row_height = 5;
// 行高
var cart_offset = 90;
// 底部栏偏移量
var product_row_height = 49;
Page({
  data: {
    categoryStates: [],
    cartData: {},
    cartObjects: [],
    maskVisual: 'hidden',
    amount: 0,
    quantity:0,
    typelist:[],
    productlist: [],
    productlistAll: [],
    root: app.rootUrl,
    shop: {},
    notice: {},
    typeId: '',
    title:'',
    circular: true, 
    //选中点的颜色
    //是否竖直
    vertical: false,
    //是否自动切换
    autoplay: true,
    //自动切换的间隔
    interval: 3000,
    //滑动动画时长毫秒
    duration: 1000,
    //所有图片的高度
    imgheights: [],
    //图片宽度
    imgwidth: 320,
    //默认
    current: 0,
    imgs:['/images/index1.jpg','/images/index1.jpg']
  },
  onLoad: function () {
    that = this;
    that.setData({
      categoryObjects: []
    });
  },
  onShow: function () {
    this.goSearch()
  },
  changeTitle: function(event) {
    let that = this;
    var inputSearch = event.detail.value;
    that.setData({
      title: inputSearch
    })
  },
  goSearch: function(){
    let  that = this;
    wx.request({
        url: app.rootUrl + '/api/indexList',
        data: {typeId:that.data.typeId, title:that.data.title},
        header: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        method: 'POST',
        success: function (res) {
          that.setData({
            typelist: res.data.typeList || [],
            productList: res.data.productList || [],
            productlistAll: res.data.productList || []
          });
        }
      })
  },
  saveCar: function (e) {
    var productid = e.currentTarget.dataset.index;
    wx.request({
      url: app.rootUrl + '/api/saveCar',
      data: {'productId':productid},
      header: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      method: 'POST',
      success: function (res) {
        wx.showToast({
            title: '已添加'
          })
      }
    });
  },
  switchCategory: function (e) {
    // 获取分类id并切换分类
    var index = e.currentTarget.dataset.index;
    var productList = [];
    var list = this.data.productlistAll;
    for (var x = 0; x < list.length;x++){
      var typeid = list[x].type.typeId;
      if (typeid == index){
        productList.push(list[x]);
      }
    }
    this.setData({
      productList: productList,
      typeId: index
    });
  },
  productDetail: function (e){
    var index = e.currentTarget.dataset.index;
    wx.navigateTo({
      url: '../detail/detail?productid=' + index,
    })
  },
  imageLoad: function(e) { //获取图片真实宽度
    var imgwidth = e.detail.width,
      imgheight = e.detail.height,
      //宽高比
      ratio = imgwidth / imgheight;
    //计算的高度值
    var viewHeight = 750 / ratio;
    var imgheight = viewHeight;
    var imgheights = this.data.imgheights;
    //把每一张图片的对应的高度记录到数组里
    imgheights[e.target.dataset.id] = imgheight;
    this.setData({
      imgheights: imgheights
    })
  },
  bindchange: function(e) {
    // console.log(e.detail.current)
    this.setData({
      current: e.detail.current
    })
  }
})
