var app = getApp();
Page({
  data: {
    userInfo: app.accountInfo(),
    canIUseGetUserProfile: false,
  },
  onLoad() {
    var that = this;
    if (wx.getUserProfile) {
      var info = app.accountInfo();
      that.setData({
        canIUseGetUserProfile: true,
        userInfo: info
      })
    }
  },
  toInfo(){
    wx.reLaunch({
      url: '../user/user',
    })
  },
  getUserProfile(e) {
    var that = this;
    wx.getUserProfile({
      desc: '用于完善会员资料',
      success: function (profileres){
        /*that.setData({
          userInfo: profileres.userInfo,
          hasUserInfo: true
        });*/
        wx.login({
          success (loginres) {
            //登录请求开始
            console.log(loginres.code)
            console.log(profileres.encryptedData)
            console.log(profileres.iv)
            console.log(app.rootUrl)
            wx.request({
              url: app.rootUrl + 'api/apiLogin',
              data: {
                'code': loginres.code,
                'encryptedData': profileres.encryptedData,
                'iv': profileres.iv
              },
            header: {
              'Content-Type': 'application/x-www-form-urlencoded',
              //'Accept': 'application/json;charset=utf-8',
            },
            method: 'POST',
              success: function (res) {
                wx.hideLoading();
                if (res.data.state == 0) {
                  wx.showToast({
                    title: res.data.msg
                  })
                  return false;
                }
                wx.showToast({
                  title: '登录成功'
                })
                wx.setStorage({
                  key:"accountInfo",
                  data: res.data.data,
                });
                wx.reLaunch({
                  url: '../user/user',
                })
              },
              fail: function (res) {
                console.log(res)
              }
            })//登录请求结束
          }
        })
      }
    })
  },
})
