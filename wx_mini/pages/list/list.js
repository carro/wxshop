// pages/list/list.js
const app = getApp()
var that;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    visual: 'hidden',
    orderList:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    that = this;
    
  },

  showDetail: function (e) {
    var index = e.currentTarget.dataset.index;
    // 传递订单objectId
    wx.navigateTo({
      url: '../orders/orders?ordersid=' + index 
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if(app.accountInfo() !== null){
      wx.request({
        url: app.rootUrl + '/api/ordersList',
        data: { 'accountId': app.accountInfo().accountId, 'state': -1 },
        header: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        method: 'POST',
        success: function (res) {
          that.setData({
            orderList: res.data.data
          });
        }
      })
    }
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})