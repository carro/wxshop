// pages/detail/detail.js
const app = getApp();
var that;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    product:{},
    commentList: [],
    root: app.rootUrl
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    that = this;
    var productid = options.productid;
    wx.request({
      url: app.rootUrl + '/api/productDetail',
      data: { 'productId': productid },
      header: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      method: 'POST',
      success: function (res) {
        that.setData({
          product: res.data
        });
      }
    })
  },
  saveCar: function (e) {
    var productid = e.currentTarget.dataset.index;
    wx.request({
      url: app.rootUrl + '/api/saveCar',
      data: {'productId':productid},
      header: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      method: 'POST',
      success: function (res) {
        wx.showToast({
            title: '已添加'
          })
      }
    });
  },
  addcomment: function (e) {
    var index = e.currentTarget.dataset.index;
    var that = this;
    if(app.accountInfo() !== null){
      wx.request({
        url: app.rootUrl + '/api/saveCollect',
        data: { 'accountId': app.accountInfo().accountId, 'productId':index },
        header: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        method: 'POST',
        success: function (res) {
          wx.showToast({
            title: res.data.msg,
          })
        }
      })
    }else{
      wx.showToast({
        title: '请登录',
      })
    }
  },

  /* saveCar: function (e) {
    var index = e.currentTarget.dataset.index;
    wx.request({
      url: app.rootUrl + '/api/saveCar',
      data: { 'diancanproduct.id': index },
      header: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      method: 'POST',
      success: function (res) {
        wx.showToast({
          title: '已添加',
        })
      }
    });
  }, */
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})