// pages/checkout/checkout.js
const app = getApp();
var that;
Page({
 
  /**
   * 页面的初始数据
   */
  data: {
    carts: [],
    amount: 0,
    quantity: 0,
    total: 0,
    hot: 0,
    way:0,
    address:'',
	deskList: [1,2,3,4,5,6,7,8,9,10],
	desk: "bs"+new Date().getTime()
  },
  changeSearch: function(event) {
    let that = this;
    var inputSearch = event.detail.value;
    that.setData({
      address: inputSearch
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    that = this;
    that.showCar();
  },

  showCar: function () {
    wx.request({
      url: app.rootUrl + '/api/showCar',
      data: {},
      header: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      method: 'POST',
      success: function (res) {
        that.setData({
          total: res.data.total,
          quantity: res.data.count,
          carts: res.data.carObjList,
          hot:res.data.hot
        });
        var val = 2000;
        if(res.data.hot*1 >= val){
          wx.showLoading({
            title: '提示超过'+val+'千焦',
          })
          setTimeout(function () {
            wx.hideLoading()
          }, 3000)
        }
      }
    })
  },
  naviToRemark: function(){
    that.setData({
      way: that.data.way==1?0:1
    });
  },
  bindPickerChange: function(e){
    var desk = e.detail.value;
	that.setData({
      desk: desk
    });
  },
  payment: function () {
    var that = this;
    if(app.accountInfo() === null){
      wx.showToast({
				title: '请登录',
				image: '../../images/info.png',
			  });
			  return null;
    }
		if(that.data.address == ''){
			wx.showToast({
				title: '地址为空',
				image: '../../images/info.png',
			  });
			  return null;
		}
	wx.request({
      url: app.rootUrl + '/api/saveCarOrders',
      data: { 'accountId': app.accountInfo().accountId,'address':that.data.address},
      header: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      method: 'POST',
      success: function (res) {
        if (res.data.state == 1) {
          wx.showModal({
            title: '成功',
            success: function () {
              //wx.switchTab({
                //url: '../list/list'
              //})
              wx.navigateTo({
                url: '../orders/orders?ordersid=' + res.data.data
              })
            }
          });
        } else {
          wx.showToast({
            title: '失败',
            image: '../../images/info.png',
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})