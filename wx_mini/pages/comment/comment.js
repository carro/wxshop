// pages/comment/comment.js
const app = getApp();
var that;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    productid : null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    that = this;
    that.setData({
      productid: options.productid
    });
  },
  setRemark: function(e){
    var remark = e.detail.value.remark;
    if (remark == '' || remark == null){
      wx.showToast({
        title: '请输入',
        image: '../../images/info.png',
      })
        return null;
    }
    wx.request({
      url: app.rootUrl + '/api/saveComment',
      data: { 'diancanComment.accountid': app.accountInfo().accountId,
       'diancanComment.productid': that.data.productid,
        'diancanComment.content': remark},
      header: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      method: 'POST',
      success: function (res) {
        if(res.data.state==1){
          wx.showModal({
            title: '成功',
            success: function () {
              var pages = getCurrentPages();
              var prevPage = pages[1];//上一个页面
              var cl = prevPage.data.commentList;
              cl.push(res.data.data);
              prevPage.setData({
                ['commentList']: cl
              });
              wx.navigateBack();
            }
          });
        }else{
          wx.showToast({
            title: '失败',
            image: '../../images/info.png',
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})