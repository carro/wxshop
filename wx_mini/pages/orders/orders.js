// pages/orders/orders.js
const app = getApp();
var that;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    order: {},
    root: app.rootUrl + '/asset/upload/',
    title:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    that = this;
    var ordersid = options.ordersid;
    that.orderDetail(ordersid)
  },
  orderDetail: function (ordersid) {
    that = this;
    wx.request({
      url: app.rootUrl + '/api/ordersDetail',
      data: { 'ordersId': ordersid },
      header: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      method: 'POST',
      success: function (res) {
        that.setData({
          order: res.data
        });
      }
    })
  },

  updateState: function (e) {
    var state = e.currentTarget.dataset.index;
    wx.request({
      url: app.rootUrl + '/api/updateOrdersState',
      data: { 'ordersId': that.data.order.id,'state': state },
      header: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      method: 'POST',
      success: function (res) {
        if (res.data.state == 1) {
          wx.showModal({
            title: '成功',
            success: function () {
              wx.navigateTo({
                url: '../list/list'
              })
            }
          });
        } else {
          wx.showToast({
            title: '失败',
            image: '../../images/info.png',
          })
        }
      }
    })
  },
  changeCommon: function(event) {
    let that = this;
    var inputSearch = event.detail.value;
    that.setData({
      title: inputSearch
    })
  },
  openCommon: function(e){
    let  that = this;
    var productid = e.currentTarget.dataset.productid;
    if(that.data.order.state !== 3){
        wx.showToast({
        title: '订单确认后可评论',
        image: '../../images/info.png',
        })
        return false;
    }
    wx.showModal({
        title: '请输入评论',
        editable: true,
        content: '',
        success (res) {
          if (res.confirm) {
            that.saveCommon(productid, res.content)
          }
        }
      })
  },
  saveCommon: function(productid, text){
    let  that = this;
    if(!text){
        wx.showToast({
        title: '请输入内容',
        image: '../../images/info.png',
        })
        return false;
    }
    wx.request({
        url: app.rootUrl + '/api/commonSave',
        data: {
            accountId:app.accountInfo().accountId, 
            ordersId:that.data.order.id, 
            productId:productid, 
            content:text
        },
        header: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        method: 'POST',
        success: function (res) {
            if (res.data.state == 1) {
                that.orderDetail(that.data.order.id)
            }
            wx.showToast({
              title: res.data.msg,
              image: '../../images/info.png',
            })
        }
      })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})