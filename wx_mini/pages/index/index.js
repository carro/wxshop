//index.js
//获取应用实例
const app = getApp();
var Bmob = require('../../utils/bmob.js');
var that;
// 最大行数
var max_row_height = 5;
// 行高
var cart_offset = 90;
// 底部栏偏移量
var product_row_height = 49;
Page({
  data: {
    categoryStates: [],
    cartData: {},
    cartObjects: [],
    maskVisual: 'hidden',
    amount: 0,
    quantity:0,
    hotList: [],
    root: app.rootUrl,
    shop: {},
    notice: {},
    typeId: '',
    circular: true,
    //是否显示画板指示点，根据图片数量自动生成多少个圆点
    indicatorDots: true,
    //选中点的颜色
    //是否竖直
    vertical: false,
    //是否自动切换
    autoplay: true,
    //自动切换的间隔
    interval: 3000,
    //滑动动画时长毫秒
    duration: 1000,
    //所有图片的高度
    imgheights: [],
    //图片宽度
    imgwidth: 320,
    //默认
    current: 0,
    imgs:['/images/index1.jpg','/images/index2.jpg'],
    title: ''
  },
  onLoad: function () {
    that = this;
     
    that.setData({
      categoryObjects: []
    });
  },
  onShow: function () {
    wx.request({
      url: app.rootUrl + '/api/hotList',
      data: {},
      header: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      method: 'POST',
      success: function (res) {
        that.setData({
            hotList: res.data
        });
      }
    })
  },
  goSearch: function(){
    that = this;
    wx.request({
        url: app.rootUrl + '/api/hotList',
        data: {title: that.data.title},
        header: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        method: 'POST',
        success: function (res) {
          that.setData({
              hotList: res.data
          });
        }
      })
  },
  changeTitle: function(event) {
    let that = this;
    var inputSearch = event.detail.value;
    that.setData({
      title: inputSearch
    })
  },
  productDetail: function (e){
    var index = e.currentTarget.dataset.index;
    wx.navigateTo({
      url: '../detail/detail?productid=' + index,
    })
  },
  imageLoad: function(e) { //获取图片真实宽度
    var imgwidth = e.detail.width,
      imgheight = e.detail.height,
      //宽高比
      ratio = imgwidth / imgheight;
    //计算的高度值
    var viewHeight = 750 / ratio;
    var imgheight = viewHeight;
    var imgheights = this.data.imgheights;
    //把每一张图片的对应的高度记录到数组里
    imgheights[e.target.dataset.id] = imgheight;
    this.setData({
      imgheights: imgheights
    })
  },
  bindchange: function(e) {
    // console.log(e.detail.current)
    this.setData({
      current: e.detail.current
    })
  }
})
