$(document).ready(function(){
	$(".quickgo-date").flatpickr({
		
	});
	$(".quickgo-date-time").flatpickr({
		enableTime: true,
		utc: false
	});
});
function QuickgoUpload(uploadObjDom, save_id, url, url2) {
	var fileObjDom = $(uploadObjDom);
	var filePath = fileObjDom.val();
	if (!filePath) {
		layer.msg('请选择文件');
		return null;
	}
	var fileType = filePath.substr(filePath.lastIndexOf(".") + 1);
	var formData = new FormData();
	formData.append('file', fileObjDom[0].files[0]);
	formData.append('fileType', fileType);
	$.ajax({
		type : 'post',
		cache : false,
		url : url,
		data : formData,
		dataType : 'json',
		processData : false,
		contentType : false,
		beforeSend : function() {
			layer.load();
		},
		success : function(res) {
			layer.closeAll();
			if (res.status == 1) {
				layer.msg("上传成功");
				$(uploadObjDom).parents(".quickgo-file-main").children("img").attr("src", res.url);
				$("#" + save_id).val(res.url);
				$(uploadObjDom).parents("button").children("span").text("上传成功");
			} else {
				layer.msg("上传失败");
				$(uploadObjDom).parents("button").children("span").text("上传失败");
			}
		},
		error : function(res, ajaxOptions, thrownError) {
			layer.closeAll();
			layer.msg('请求失败');
		}
	});
}