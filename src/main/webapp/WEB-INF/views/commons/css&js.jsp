<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/commons/tags.jsp" %>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="${ctx}/static/admin_4/pages/css/googleapis.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/static/admin_4/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/static/admin_4/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/static/admin_4/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/static/admin_4/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="${ctx}/static/admin_4/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="${ctx}/static/admin_4/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<!-- BEGIN THEME LAYOUT STYLES -->
<link href="${ctx}/static/admin_4/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/static/admin_4/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
<link href="${ctx}/static/admin_4/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME LAYOUT STYLES -->
<!-- END QUICK NAV -->
<!--[if lt IE 9]>
<script src="${ctx}/static/admin_4/global/plugins/respond.min.js"></script>
<script src="${ctx}/static/admin_4/global/plugins/excanvas.min.js"></script> 
<script src="${ctx}/static/admin_4/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="${ctx}/static/admin_4/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/jquery.cookie.min.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<link href="${ctx}/static/admin_4/global/plugins/bootstrap-touchspin/bootstrap.touchspin.css" rel="stylesheet" type="text/css" />
<script src="${ctx}/static/admin_4/global/plugins/fuelux/js/spinner.min.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js" type="text/javascript"></script>
<!-- jquery-validation -->
<script src="${ctx}/static/admin_4/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/jquery-validation/js/localization/messages_zh.js" type="text/javascript"></script>
<!-- bootstrap-sweetalert -->
<link href="${ctx}/static/admin_4/global/plugins/bootstrap-sweetalert/sweetalert2.css" rel="stylesheet" type="text/css" />
<script src="${ctx}/static/admin_4/global/plugins/bootstrap-sweetalert/sweetalert2.min.js" type="text/javascript"></script>
<!-- bootstrap-table -->
<script src="${ctx}/static/admin_4/global/plugins/bootstrap-table/bootstrap-table.min.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/bootstrap-table/extensions/export/bootstrap-table-export.min.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/bootstrap-table/extensions/export/tableExport-master.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/bootstrap-table/locale/bootstrap-table-zh-CN.min.js" type="text/javascript"></script>
<!-- bootstrap-fileinput -->
<link href="${ctx}/static/admin_4/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<script src="${ctx}/static/admin_4/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<!-- summernote -->
<link href="${ctx}/static/admin_4/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
<script src="${ctx}/static/admin_4/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/bootstrap-summernote/lang/summernote-zh-CN.min.js" type="text/javascript"></script>
<!-- umeditor -->
<%--<script type="text/javascript" src="${ctx}/static/admin_4/global/plugins/umeditor/umeditor.config.js"></script>
<script type="text/javascript" src="${ctx}/static/admin_4/global/plugins/umeditor/umeditor.js"></script>
--%><!-- select2 -->
<link href="${ctx}/static/admin_4/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/static/admin_4/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="${ctx}/static/admin_4/global/plugins/select2/js/select2.min.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/select2/js/i18n/zh-CN.js" type="text/javascript"></script>
<!-- bootstrap-toastr -->
<link href="${ctx}/static/admin_4/global/plugins/bootstrap-toastr/toastr.min.css" rel="stylesheet" type="text/css" />
<script src="${ctx}/static/admin_4/global/plugins/bootstrap-toastr/toastr.min.js" type="text/javascript"></script>
<!--layui-->
<link href="${ctx}/static/dist/css/layui.css" rel="stylesheet" type="text/css" />
<script src="${ctx}/static/dist/layui.all.js" type="text/javascript"></script>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="${ctx}/static/admin_4/global/plugins/bootstrap-colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css" />
<script src="${ctx}/static/admin_4/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js" type="text/javascript"></script>
<!-- jquery fileupload -->
<%-- <link href="${ctx}/static/admin_4/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/static/admin_4/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/static/admin_4/global/plugins/jquery-file-upload/css/jquery.fileupload.css" rel="stylesheet" type="text/css" />
<link href="${ctx}/static/admin_4/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet" type="text/css" />
<script src="${ctx}/static/admin_4/global/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/jquery-file-upload/js/vendor/jquery.ui.widget.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/jquery-file-upload/js/vendor/tmpl.min.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/jquery-file-upload/js/vendor/load-image.min.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/jquery-file-upload/js/vendor/canvas-to-blob.min.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/jquery-file-upload/blueimp-gallery/jquery.blueimp-gallery.min.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/jquery-file-upload/js/jquery.iframe-transport.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/jquery-file-upload/js/jquery.fileupload.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/jquery-file-upload/js/jquery.fileupload-process.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/jquery-file-upload/js/jquery.fileupload-image.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/jquery-file-upload/js/jquery.fileupload-audio.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/jquery-file-upload/js/jquery.fileupload-video.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/jquery-file-upload/js/jquery.fileupload-validate.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/jquery-file-upload/js/jquery.fileupload-ui.js" type="text/javascript"></script> --%>
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="${ctx}/static/admin_4/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<script src="${ctx}/static/admin_4/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.zh-CN.min.js" type="text/javascript"></script>
<!-- date with time -->
<link href="${ctx}/static/admin_4/global/plugins/bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<script src="${ctx}/static/admin_4/global/plugins/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.js" charset="UTF-8" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/global/plugins/bootstrap-datetimepicker-master/js/locales/bootstrap-datetimepicker.zh-CN.js" charset="UTF-8" type="text/javascript"></script>
 <!-- BEGIN PAGE LEVEL SCRIPTS -->
<link href="${ctx}/static/admin_4/pages/css/login.min.css" rel="stylesheet" type="text/css" />
 <script src="${ctx}/static/admin_4/pages/scripts/login.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="${ctx}/static/admin_4/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="${ctx}/static/admin_4/layouts/layout4/scripts/layout.min.js" type="text/javascript"></script>
<script src="${ctx}/static/admin_4/layouts/layout4/scripts/demo.min.js" type="text/javascript"></script>
<!-- photoClip -->
<script type="text/javascript" src="${ctx}/static/admin_4/global/plugins/photoClip/hammer.min.js"></script>
<script type="text/javascript" src="${ctx}/static/admin_4/global/plugins/photoClip/lrz.all.bundle.js"></script>
<script type="text/javascript" src="${ctx}/static/admin_4/global/plugins/photoClip/iscroll-zoom-min.js"></script>
<script type="text/javascript" src="${ctx}/static/admin_4/global/plugins/photoClip/PhotoClip.js"></script>
<!-- data-time -->
<link href="${ctx}/static/dist/date-time/dist/flatpickr.min.css" rel="stylesheet" type="text/css">
<script src="${ctx}/static/dist/date-time/dist/flatpickr.min.js"></script>
<script src="${ctx}/static/dist/date-time/src/flatpickr.l10n.zh.js"></script>
<!-- mime -->
<link rel="stylesheet" type="text/css" href="${ctx}/static/dist/css.css">
<script type="text/javascript" src="${ctx}/static/dist/js.js"></script>
<!-- END THEME LAYOUT SCRIPTS -->
<link rel="shortcut icon" href="${ctx}/static/favicon.ico" />
