<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>500</title>
        <%@ include file="/WEB-INF/views/commons/css&js.jsp" %> 
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class=" page-404-full-page">
        <div class="row">
            <div class="col-md-12 page-404">
                <div class="number font-red"> 500 </div>
                <div class="details">
                    <h3>Oops! System is error.</h3>
                    <p> 程序错误
                        <br/>
                        <a href="${ctx}/"> 返回</a>主页 </p>
                    <form action="#">
                        <div class="input-group input-medium">
                            <input type="text" class="form-control" placeholder="keyword..." disabled>
                            <span class="input-group-btn">
                                <button type="submit" class="btn red disabled">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                        <!-- /input-group -->
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>