<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/commons/tags.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>微信小店管理系统</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
    <div class="page-content-wrapper ">
	     <!-- BEGIN CONTENT BODY -->
	     <div class="page-content">
	         <!-- BEGIN PAGE HEAD-->
	         <div class="page-head">
	             <!-- BEGIN PAGE TITLE -->
	             <div class="page-title">
	                 <c:if test="${empty product}">
	                 <h1>添加商品</h1>
	                 </c:if>
	                 <c:if test="${not empty product}">
	                 <h1>修改商品</h1>
	                 </c:if>
	             </div>
	             <!-- END PAGE TITLE -->
	             <!-- BEGIN PAGE TOOLBAR -->
	             <div class="page-toolbar">
	                 
	             </div>
	             <!-- END PAGE TOOLBAR -->
	         </div>
	         <!-- END PAGE HEAD-->
	         <!-- BEGIN PAGE BASE CONTENT -->
	         <div class="page-body portlet light">
	         	<div class="row">
	         		<div class="col-md-offset-2 col-md-8">
	         			<!-- BEGIN FORM-->
			             <form action="#" id="productForm" class="form-horizontal" method="post">
			             	<input type="hidden" name="productId" value="${product.productId}" />
			                 <div class="form-body ">
			                     <div class="form-group">
			                         <label class="control-label col-md-2">类别
			                             <span class="required"> * </span>
			                         </label>
			                         <div class="col-md-6">
			                             <select data-required="1" class="form-control" name="productTypeId">
			                             	<c:forEach items="${typeList}" var="item">
			                             		<option value="${item.typeId}" <c:if test="${product.productTypeId eq item.typeId}">selected="selected"</c:if> >${item.typeTitle}</option>
			                             	</c:forEach>
			                             </select>
			                         </div>
			                     </div>
			                     <div class="form-group">
			                         <label class="control-label col-md-2">名称
			                             <span class="required"> * </span>
			                         </label>
			                         <div class="col-md-6">
			                             <input type="text" name="productTitle" data-required="1" minlength="3" maxlength="10"
			                             class="form-control" value="${product.productTitle}" />
			                         </div>
			                     </div>
			                     <div class="form-group">
			                         <label class="control-label col-md-2">编号
			                             <span class="required"> * </span>
			                         </label>
			                         <div class="col-md-6">
			                             <input type="text" name="productCode" data-required="1" class="form-control" value="${product.productCode}" />
			                         </div>
			                     </div>
			                     <div class="form-group">
			                         <label class="control-label col-md-2">品牌
			                             <span class="required"> * </span>
			                         </label>
			                         <div class="col-md-6">
			                             <input type="text" name="productBrand" data-required="1" class="form-control" value="${product.productBrand}" />
			                         </div>
			                     </div>
			                     <div class="form-group">
			                         <label class="control-label col-md-2">单价
			                             <span class="required"> * </span>
			                         </label>
			                         <div class="col-md-6">
			                             <input type="text" name="productPrice" data-required="1" class="form-control" value="${product.productPrice}" />
			                         </div>
			                     </div>
			                     <div class="form-group">
			                         <label class="control-label col-md-2">上市时间
			                             <span class="required"> * </span>
			                         </label>
			                         <div class="col-md-6">
			                             <input type="text" name="productPublish" data-required="1" class="form-control flatpickr" value="${product.productPublish}" />
			                         </div>
			                     </div>
			                     <div class="form-group">
			                         <label class="control-label col-md-2">规格
			                             <span class="required"> * </span>
			                         </label>
			                         <div class="col-md-6">
			                             <input type="text" name="productSubtitle" data-required="1" class="form-control" value="${product.productSubtitle}" />
			                         </div>
			                     </div>
			                     <div class="form-group">
			                         <label class="control-label col-md-2">热门推荐
			                             <span class="required"> * </span>
			                         </label>
			                         <div class="col-md-6">
			                             <select data-required="1" class="form-control" name="productHot">
			                             	<option value="1" <c:if test="${product.productHot eq 1}">selected="selected"</c:if> >上热门</option>
			                             	<option value="0" <c:if test="${product.productHot eq 0}">selected="selected"</c:if> >无</option>
			                             </select>
			                         </div>
			                     </div>
			                     <div class="form-group">
			                         <label class="control-label col-md-2">封面
			                             <span class="required"> * </span>
			                         </label>
			                         <div class="col-md-6">
			                         	<div class="quickgo-file-main">
											<img src="${ctx}${product.productLogo}" alt="封面预览">
											<div>
												<button>
													<span>选择图片</span>
													<input type="file" onchange="QuickgoUpload(this, 'productLogo','${ctx}/file/uploadFile','${ctx}');">
												</button>
											</div>
										</div>
			                         	<input type="hidden" name="productLogo" id="productLogo" data-required="1" class="form-control" value="${product.productLogo}" />
			                         </div>
			                     </div>
			                     <div class="form-group">
			                         <label class="control-label col-md-2">产品描述
			                             <span class="required"> * </span>
			                         </label>
			                         <div class="col-md-6">
			                             <textarea rows="10" style="width:100%;" class="form-control" name="productContent" >${product.productContent}</textarea>
			                         </div>
			                     </div>
			                     <div class="col-md-offset-4 margin-top-10">
			                         <button type="reset" class="btn default" onclick="history.go(-1)"> 取消 </button>
			                         &nbsp;&nbsp;&nbsp;&nbsp;
			                         <button type="submit" class="btn green"> 保存 </button>
			                     </div>
			                 </div>
			             </form>
			             <!-- END FORM-->
	         		</div>
	         	</div>
	         </div>
	     </div>
	     <!-- END CONTENT BODY -->
	 </div>
	 <script type="text/javascript">
	 jQuery.validator.addMethod("enNumber", function(value, element) {
		 	return this.optional(element) || /^[a-zA-Z0-9]+$/.test(value);
		 }, "请输入英文/数字");
	 jQuery.validator.addMethod("isPrice",function(value, element){
	         var returnVal = true;
	         inputZ=value;
	         var ArrMen= inputZ.split(".");    //截取字符串
	         if(ArrMen.length==2){
	             if(ArrMen[1].length>2){    //判断小数点后面的字符串长度
	                 returnVal = false;
	                 return false;
	             }
	         }
	         return returnVal;
	     },"请输入正确价格");
	 $.extend($.validator.messages, {
		 minlength: $.validator.format("请输入至少 {0} 个字符！"),
		 maxlength: $.validator.format("请输入不超过 {0} 个字符！")
		 });
	 $(function(){
		 $(".flatpickr").flatpickr({
				
		 });
	     var productForm = $('#productForm');
	     var error = $('.alert-danger', productForm);
	     var success = $('.alert-success', productForm);
	     var updateAccountForm = productForm.validate({
	         errorElement: 'span', 
	         errorClass: 'help-block help-block-error',
	         focusInvalid: false,
	         ignore: ".ignore",
	         messages: {
	        	 productTypeId: {required:'请输入'},
	        	 productTitle: {required:'请输入'},
	        	 productCode: {required:'请输入'},
	        	 productBrand: {required:'请输入'},
	        	 productPrice: {required:'请输入'},
	        	 productPublish: {required:'请输入'},
	        	 productSubtitle: {required:'请输入'},
	        	 productContent: {required:'请输入'},
	        	 productLogo: {required:'请上传'},
	         },
	         rules: {
	        	 productTypeId: {required:true},
	        	 productTitle: {required:true},
	        	 productCode: {required:true, enNumber:true},
	        	 productBrand: {required:true},
	        	 productPrice: {required:true, isPrice:true},
	        	 productPublish: {required:true},
	        	 productSubtitle: {required:true},
	        	 productContent: {required:true},
	        	 productLogo: {required:true},
	         },
	         highlight: function (element) {
	             $(element).closest('.form-group').addClass('has-error'); 
	         },
	         unhighlight: function (element) {
	             $(element).closest('.form-group').removeClass('has-error');
	         },
	         success: function (label) {
	             label.closest('.form-group').removeClass('has-error');
	         },
	         submitHandler: function (form) {
	         	var flag = productForm.valid();
	         	$.ajax({
	                 type: "post",
	                 cache: false,
	                 url: '${ctx}/admin/saveProduct',
	                 data: productForm.serialize(),
	                 dataType: 'json',
	                 success: function (res) {    
	                     swal(res.data, '', res.msg);
	                 },
	                 error: function (res, ajaxOptions, thrownError) {
	                 	alert('无法加载...');
	                 }
	             });
	         }
	     });
		});
	 </script>
  </body>
</html>
