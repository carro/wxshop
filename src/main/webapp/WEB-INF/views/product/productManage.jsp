<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/commons/tags.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>微信小店管理系统</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
    <div class="page-content-wrapper ">
	     <!-- BEGIN CONTENT BODY -->
	     <div class="page-content">
	         <!-- BEGIN PAGE HEAD-->
	         <div class="page-head">
	             <!-- BEGIN PAGE TITLE -->
	             <div class="page-title">
	                 <h1>商品列表
	                     <small>商品管理</small>
	                 </h1>
	             </div>
	             <!-- END PAGE TITLE -->
	             <!-- BEGIN PAGE TOOLBAR -->
	             <div class="page-toolbar">
	                 
	             </div>
	             <!-- END PAGE TOOLBAR -->
	         </div>
	         <!-- END PAGE HEAD-->
	         <!-- BEGIN PAGE BASE CONTENT -->
	         <div class="page-body portlet light">
	         	<div class="portlet-body">
	         		<form class="form-inline" role="form">
					  <div class="form-group col-md-2 col-md-offset-3">
					    <select class="form-control" id="searchTypeId" name="searchTypeId">
                        	<option value="">全部类别</option>
                        	<c:forEach items="${typeList}" var="item">
                        		<option <c:if test="${param.searchTypeId eq item.typeId}">selected="selected"</c:if> value="${item.typeId}">${item.typeTitle}</option>
                        	</c:forEach>
                        </select>
					  </div>
					  <div class="form-group col-md-2">
					    <input class="form-control" id="searchTitle" name="searchTitle" placeholder="商品标题" value="${param.searchTitle}">
					  </div>
					  <div class="form-group col-md-2">
					    <button type="submit" class="btn btn-primary">搜索</button>
					  </div>
					</form>
	         	</div>
	         	<div class="portlet-body">
	              <table id="productTable" class="table"></table>
	          	</div>
	         </div>
	     </div>
	     <!-- END CONTENT BODY -->
	 </div>
	 <script type="text/javascript">
	 	productTable();
		function productTable(){
			$('#productTable').bootstrapTable('destroy'); 
			$('#productTable').bootstrapTable({
			url:'${ctx}/admin/getProduct',
			queryParams :{
				searchTypeId:'${param.searchTypeId}',
				searchTitle:'${param.searchTitle}'},
			uniqueId: 'typeId',
			dataType:'json',
			striped: true,
			pagination:true,
			pageNumber:1,                       //初始化加载第一页，默认第一页
		    pageSize: 10,                       //每页的记录行数（*）
		    pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
		    columns: [
				{
					field : '',
					title : '类别',
					align : 'center',
					valign : 'middle',
					formatter : function(value, row, index) {
						if(row.type !=null){
							return row.type.typeTitle;
						}
					}
				},
				{
					field : 'productTitle',
					title : '标题',
					align : 'center',
					valign : 'middle'
				},
				{
					field : 'productLogo',
					title : '封面',
					align : 'center',
					valign : 'middle',
					formatter : function(value, row, index) {
						if(value){
							return '<img src="${ctx}'+value+'" style="width:60px;height:60px;"/>';
						}
					}
				},
				{
					field : 'productCode',
					title : '编号',
					align : 'center',
					valign : 'middle',
				},
				{
					field : 'productPrice',
					title : '单价',
					align : 'center',
					valign : 'middle',
				},
				{
					field : 'productPublish',
					title : '上市时间',
					align : 'center',
					valign : 'middle'
				},
				{
					field : 'productSubtitle',
					title : '规格',
					align : 'center',
					valign : 'middle'
				},
				{
					field : 'productHot',
					title : '热门推荐',
					align : 'center',
					valign : 'middle'
				},
				{
					field : 'productTime',
					title : '创建时间',
					align : 'center',
					valign : 'middle'
				},
				{
					field : 'productId',
					title : '操作',
					align : 'center',
					valign : 'middle',
					formatter : function(value, row, index) {
						var del = '<button type="button" class="btn btn-xs btn-danger" onclick="del(\''+value+'\');">删除</button>';
						var edit = '<button type="button" class="btn btn-xs btn-warning" onclick="window.location.href=\'${ctx}/admin/productCreate?productId='+value+'\';">编辑</button>';
						return edit+del;
					}
				},
			]
			});
		}
		function del(id){
			$.ajax({
                type: "post",
                cache: false,
                url: '${ctx}/admin/deleteProduct',
                data:{'productId':id},
                dataType: 'json',
                success: function (res) {    
                    swal(res.data, '', res.msg);
                    if(res.state==1){
                    	productTable();
                    }
                },
                error: function (res, ajaxOptions, thrownError) {
                	alert('页面无法加载...');
                }
            });
		}
	 </script>
  </body>
</html>
