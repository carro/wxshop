<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/commons/tags.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>微信小店管理系统</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
    <div class="page-content-wrapper ">
	     <!-- BEGIN CONTENT BODY -->
	     <div class="page-content">
	         <!-- BEGIN PAGE HEAD-->
	         <div class="page-head">
	             <!-- BEGIN PAGE TITLE -->
	             <div class="page-title">
	                 <h1>评价列表
	                     <small>评价管理</small>
	                 </h1>
	             </div>
	             <!-- END PAGE TITLE -->
	             <!-- BEGIN PAGE TOOLBAR -->
	             <div class="page-toolbar">
	                 
	             </div>
	             <!-- END PAGE TOOLBAR -->
	         </div>
	         <!-- END PAGE HEAD-->
	         <!-- BEGIN PAGE BASE CONTENT -->
	         <div class="page-body portlet light">
	         	<%-- <div class="portlet-body">
	         		<form class="form-inline" role="form">
					  <div class="form-group col-md-1">
					    <button type="button" class="btn btn-default" style="border-color: #ffffff;">查询</button>
					  </div>
					  <div class="form-group col-md-offset-3 col-md-3">
					    <select class="form-control" id="searchDepartId" name="searchDepartId">
                        	<option value="">请选择系别</option>
                        	<c:forEach items="${departList}" var="item">
                        		<option <c:if test="${param.searchDepartId eq item.departId}">selected="selected"</c:if> value="${item.departId}">${item.departTitle}</option>
                        	</c:forEach>
                        </select>
					  </div>
					  <div class="form-group col-md-2">
					    <button type="submit" class="btn btn-primary">确定</button>
					  </div>
					</form>
	         	</div> --%>
	         	<div class="portlet-body">
	              <table id="commonTable" class="table"></table>
	          	</div>
	         </div>
	     </div>
	     <!-- END CONTENT BODY -->
	 </div>
	 <script type="text/javascript">
	 	commonTable();
		function commonTable(){
			$('#commonTable').bootstrapTable('destroy'); 
			$('#commonTable').bootstrapTable({
			url:'${ctx}/admin/commonList',
			queryParams :{},
			uniqueId: 'commonId',
			dataType:'json',
			striped: true,
			pagination:true,
			pageNumber:1,                       //初始化加载第一页，默认第一页
		    pageSize: 10,                       //每页的记录行数（*）
		    pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
		    columns: [
				{
					field : 'accountNick',
					title : '评论人',
					align : 'center',
					valign : 'middle'
				},
				{
					field : 'productTitle',
					title : '评论商品',
					align : 'center',
					valign : 'middle'
				},
				{
					field : 'commonContent',
					title : '评论内容',
					align : 'center',
					valign : 'middle',
				},
				{
					field : 'commonTime',
					title : '评论时间',
					align : 'center',
					valign : 'middle',
				},
				{
					field : 'commonId',
					title : '操作',
					align : 'center',
					valign : 'middle',
					formatter : function(value, row, index) {
						var del = '<button type="button" class="btn btn-xs btn-danger" onclick="del(\''+value+'\');">删除</button>';
						return del;
					}
				},
			]
			});
		}
		function del(id){
			$.ajax({
                type: "post",
                cache: false,
                url: '${ctx}/admin/deleteCommon',
                data:{'commonId':id},
                dataType: 'json',
                success: function (res) {
                    swal(res.data, '', res.msg);
                    if(res.state==1){
                    	commonTable();
                    }
                },
                error: function (res, ajaxOptions, thrownError) {
                	alert('页面无法加载...');
                }
            });
		}
	 </script>
  </body>
</html>
