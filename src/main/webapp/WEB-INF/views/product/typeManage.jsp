<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/commons/tags.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>微信小店管理系统</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
    <div class="page-content-wrapper ">
	     <!-- BEGIN CONTENT BODY -->
	     <div class="page-content">
	         <!-- BEGIN PAGE HEAD-->
	         <div class="page-head">
	             <!-- BEGIN PAGE TITLE -->
	             <div class="page-title">
	                 <h1>商品类型列表
	                     <small>商品类型管理</small>
	                 </h1>
	             </div>
	             <!-- END PAGE TITLE -->
	             <!-- BEGIN PAGE TOOLBAR -->
	             <div class="page-toolbar">
	                 
	             </div>
	             <!-- END PAGE TOOLBAR -->
	         </div>
	         <!-- END PAGE HEAD-->
	         <!-- BEGIN PAGE BASE CONTENT -->
	         <div class="page-body portlet light">
	         	<div class="portlet-body">
	         	<a class="btn btn-primary btn-md" href="${ctx}/admin/typeCreate">新增</a>
	              <table id="typeTable" class="table"></table>
	          	</div>
	         </div>
	     </div>
	     <!-- END CONTENT BODY -->
	 </div>
	 <script type="text/javascript">
	 	typeTable();
		function typeTable(){
			$('#typeTable').bootstrapTable('destroy'); 
			$('#typeTable').bootstrapTable({
			url:'${ctx}/admin/getType',
			queryParams :{},
			uniqueId: 'typeId',
			dataType:'json',
			striped: true,
			pagination:true,
			pageNumber:1,                       //初始化加载第一页，默认第一页
		    pageSize: 10,                       //每页的记录行数（*）
		    pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
		    columns: [
				{
					field : 'typeTitle',
					title : '标题',
					align : 'center',
					valign : 'middle'
				},
				{
					field : 'typeTime',
					title : '创建时间',
					align : 'center',
					valign : 'middle'
				},
				{
					field : 'typeId',
					title : '操作',
					align : 'center',
					valign : 'middle',
					formatter : function(value, row, index) {
						var del = '<button type="button" class="btn btn-xs btn-danger" onclick="del(\''+value+'\');">删除</button>';
						var edit = '<button type="button" class="btn btn-xs btn-warning" onclick="window.location.href=\'${ctx}/admin/typeCreate?typeId='+value+'\';">编辑</button>';
						return edit+del;
					}
				},
			]
			});
		}
		function del(id){
			$.ajax({
                type: "post",
                cache: false,
                url: '${ctx}/admin/deleteType',
                data:{'typeId':id},
                dataType: 'json',
                success: function (res) {    
                    swal(res.data, '', res.msg);
                    if(res.state==1){
                    	typeTable();
                    }
                },
                error: function (res, ajaxOptions, thrownError) {
                	alert('页面无法加载...');
                }
            });
		}
	 </script>
  </body>
</html>
