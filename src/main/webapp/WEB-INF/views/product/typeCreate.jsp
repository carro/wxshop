<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/commons/tags.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>微信小店管理系统</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
    <div class="page-content-wrapper ">
	     <!-- BEGIN CONTENT BODY -->
	     <div class="page-content">
	         <!-- BEGIN PAGE HEAD-->
	         <div class="page-head">
	             <!-- BEGIN PAGE TITLE -->
	             <div class="page-title">
	                 <h1>添加商品类型
	                     <small>商品类型管理</small>
	                 </h1>
	             </div>
	             <!-- END PAGE TITLE -->
	             <!-- BEGIN PAGE TOOLBAR -->
	             <div class="page-toolbar">
	                 
	             </div>
	             <!-- END PAGE TOOLBAR -->
	         </div>
	         <!-- END PAGE HEAD-->
	         <!-- BEGIN PAGE BASE CONTENT -->
	         <div class="page-body portlet light">
	         	<div class="row">
	         		<div class="col-md-offset-2 col-md-8">
	         			<!-- BEGIN FORM-->
			             <form action="#" id="typeForm" class="form-horizontal" method="post">
			             	<input type="hidden" name="typeId" value="${type.typeId}" />
			                 <div class="form-body ">
			                     <div class="form-group">
			                         <label class="control-label col-md-2">名称
			                             <span class="required"> * </span>
			                         </label>
			                         <div class="col-md-6">
			                             <input type="text" name="typeTitle" data-required="1" class="form-control" value="${type.typeTitle}" />
			                         </div>
			                     </div>
			                     <div class="col-md-offset-4 margin-top-10">
			                         <button type="reset" class="btn default" onclick="history.go(-1)"> 取消 </button>
			                         &nbsp;&nbsp;&nbsp;&nbsp;
			                         <button type="submit" class="btn green"> 保存 </button>
			                     </div>
			                 </div>
			             </form>
			             <!-- END FORM-->
	         		</div>
	         	</div>
	         </div>
	     </div>
	     <!-- END CONTENT BODY -->
	 </div>
	 <script type="text/javascript">
	 $(function(){    
	 var typeForm = $('#typeForm');
	     var error = $('.alert-danger', typeForm);
	     var success = $('.alert-success', typeForm);
	     var updateAccountForm = typeForm.validate({
	         errorElement: 'span', 
	         errorClass: 'help-block help-block-error',
	         focusInvalid: false,
	         ignore: ".ignore",
	         messages: {
	        	 typeTitle: {required:'请输入商品类别'}
	         },
	         rules: {
	        	 typeTitle: {required:true},
	         },
	         highlight: function (element) {
	             $(element).closest('.form-group').addClass('has-error'); 
	         },
	         unhighlight: function (element) {
	             $(element).closest('.form-group').removeClass('has-error');
	         },
	         success: function (label) {
	             label.closest('.form-group').removeClass('has-error');
	         },
	         submitHandler: function (form) {
	         	var flag = typeForm.valid();
	         	$.ajax({
	                 type: "post",
	                 cache: false,
	                 url: '${ctx}/admin/saveType',
	                 data: typeForm.serialize(),
	                 dataType: 'json',
	                 success: function (res) {    
	                     swal(res.data, '', res.msg);
	                 },
	                 error: function (res, ajaxOptions, thrownError) {
	                 	alert('无法加载...');
	                 }
	             });
	         }
	     });
		});
	 </script>
  </body>
</html>
