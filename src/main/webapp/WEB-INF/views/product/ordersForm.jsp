<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/page/commons/tags.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>订单记录表单</title>
</head>
<body>
<div class="container-fluid">
<div class="row bg-title">
<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
<h4 class="page-title">订单记录管理</h4> </div>
</div>
<div class="row">
<div class="col-md-10">
<div class="white-box">
<h3 class="box-title">订单记录信息设置</h3>
<form class="form-horizontal" id="form">
<input type="hidden" value="${diancanOrders.id}" name="diancanOrders.id">
<input type="hidden" value="${diancanOrders.date}" name="diancanOrders.date">
<div class="form-group">
<label class="col-md-3">菜品</label>
<div class="col-md-9">
<select id="foodid" name="diancanOrders.foodid" class="form-control ">
<c:forEach items="${diancanFoodList}" var="item">
<option value="${item.id}" <c:if test="${item.id eq diancanOrders.foodid}">selected="selected"</c:if> >${item.title}</option>
</c:forEach>
</select>
</div>
</div>
<div class="form-group">
<label class="col-md-3">购买者</label>
<div class="col-md-9">
<select id="accountid" name="diancanOrders.accountid" class="form-control ">
<c:forEach items="${accountList}" var="item">
<option value="${item.id}" <c:if test="${item.id eq diancanOrders.accountid}">selected="selected"</c:if> >${item.name}</option>
</c:forEach>
</select>
</div>
</div>
<div class="form-group">
<label class="col-md-3">订单号</label>
<div class="col-md-9">
<input type="text" id="number" name="diancanOrders.number" value="${diancanOrders.number}" class="form-control " readonly>
</div>
</div>
<div class="form-group">
<label class="col-md-3">桌号</label>
<div class="col-md-9">
<input type="text" id="desk" name="diancanOrders.desk" value="${diancanOrders.desk}" class="form-control " >
</div>
</div>
<div class="form-group">
<label class="col-md-3">下单时间</label>
<div class="col-md-9">
<input type="text" id="time" name="diancanOrders.time" value="${diancanOrders.time}" class="form-control " >
</div>
</div>
<div class="form-group">
<label class="col-md-3">数量</label>
<div class="col-md-9">
<input type="text" id="count" name="diancanOrders.count" value="${diancanOrders.count}" class="form-control " >
</div>
</div>
<div class="form-group">
<label class="col-md-3">收货地址</label>
<div class="col-md-9">
<input type="text" id="address" name="diancanOrders.address" value="${diancanOrders.address}" class="form-control " >
</div>
</div>
<div class="form-group">
<label class="col-md-3">使用方式</label>
<div class="col-md-9">
<select id="way" name="diancanOrders.way" class="form-control ">
<option value="0"<c:if test="${0 eq diancanOrders.way}">selected="selected"</c:if>>堂食</option>
<option value="1"<c:if test="${1 eq diancanOrders.way}">selected="selected"</c:if>>打包</option>
</select>
</div>
</div>
<div class="form-group">
<label class="col-md-3">状态</label>
<div class="col-md-9">
<select id="state" name="diancanOrders.state" class="form-control ">
<option value="0"<c:if test="${0 eq diancanOrders.state}">selected="selected"</c:if>>未付款</option>
<option value="1"<c:if test="${1 eq diancanOrders.state}">selected="selected"</c:if>>已付款</option>
<option value="2"<c:if test="${2 eq diancanOrders.state}">selected="selected"</c:if>>已发货</option>
<option value="3"<c:if test="${3 eq diancanOrders.state}">selected="selected"</c:if>>已完成</option>
</select>
</div>
</div>
<div class="form-group">
<div class="col-sm-12">
<button type="button" class="btn btn-success pull-right" onclick="submitForm();">保存</button>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
<script type="text/javascript">
function submitForm(){
$.ajax({
type: "post",
cache: false,
url: '${path}/admin/saveDiancanOrders',
data:$('#form').serialize(),
dataType: 'json',
success: function (res) {
layuiMsg(res.data);
},
error: function () {
alert('无法连接服务');
}
});
}
</script>
</body>
</html>

