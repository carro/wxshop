<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/commons/tags.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>微信小店管理系统</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
    <div class="page-content-wrapper ">
	     <!-- BEGIN CONTENT BODY -->
	     <div class="page-content">
	         <!-- BEGIN PAGE HEAD-->
	         <div class="page-head">
	             <!-- BEGIN PAGE TITLE -->
	             <div class="page-title">
	                 <h1>${active eq 1?'':'黑名单'}账号列表
	                     <small>${active eq 1?'':'黑名单'}账号管理</small>
	                 </h1>
	             </div>
	             <!-- END PAGE TITLE -->
	             <!-- BEGIN PAGE TOOLBAR -->
	             <div class="page-toolbar">
	                 
	             </div>
	             <!-- END PAGE TOOLBAR -->
	         </div>
	         <!-- END PAGE HEAD-->
	         <!-- BEGIN PAGE BASE CONTENT -->
	         <div class="page-body portlet light">
	         	<div class="portlet-body">
	              <table id="classesTable" class="table"></table>
	          	</div>
	         </div>
	     </div>
	     <!-- END CONTENT BODY -->
	 </div>
	 <script type="text/javascript">
	 	classesTable();
		function classesTable(){
			$('#classesTable').bootstrapTable('destroy'); 
			$('#classesTable').bootstrapTable({
			url:'${ctx}/admin/accountList',
			queryParams :{active:'${active}'},
			uniqueId: 'accountId',
			dataType:'json',
			striped: true,
			pagination:true,
			pageNumber:1,                       //初始化加载第一页，默认第一页
		    pageSize: 10,                       //每页的记录行数（*）
		    pageList: [10, 25, 50, 100],        //可供选择的每页的行数（*）
		    columns: [
				{
					field : 'accountNick',
					title : '昵称',
					align : 'center',
					valign : 'middle'
				},
				{
					field : 'accountLogo',
					title : '头像',
					align : 'center',
					valign : 'middle',
					formatter : function(value, row, index) {
						if(value){
							return '<img src="${ctx}'+value+'" style="width:60px;height:60px;"/>';
						}
					}
				},
				{
					field : 'accountSex',
					title : '性别',
					align : 'center',
					valign : 'middle',
					formatter : function(value, row, index) {
						if(row.value == 1){
							return '男';
						}
						return '女';
					}
				},
				{
					field : 'accountTime',
					title : '注册时间',
					align : 'center',
					valign : 'middle',
				},
				{
					field : 'accountId',
					title : '操作',
					align : 'center',
					valign : 'middle',
					formatter : function(value, row, index) {
						<c:if test="${active eq 1}">
						var del = '<button type="button" class="btn btn-xs btn-danger" onclick="del(\''+value+'\',0);">加入黑名单</button>';
						</c:if>
						<c:if test="${active eq 0}">
						var del = '<button type="button" class="btn btn-xs btn-danger" onclick="del(\''+value+'\',1);">激活</button>';
						</c:if>
						return del;
					}
				},
			]
			});
		}
		function del(id,active){
			$.ajax({
                type: "post",
                cache: false,
                url: '${ctx}/admin/accountActive',
                data:{'accountId':id,'active':active},
                dataType: 'json',
                success: function (res) {    
                    swal(res.data, '', res.msg);
                    if(res.state==1){
                    	classesTable();
                    }
                },
                error: function (res, ajaxOptions, thrownError) {
                	alert('页面无法加载...');
                }
            });
		}
	 </script>
  </body>
</html>
