<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/views/commons/tags.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>
		微信小店管理系统
	</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!-- <link rel="stylesheet" type="text/css" href="styles.css">
	-->
</head>
<body>
	<div class="page-content-wrapper ">
		<!-- BEGIN CONTENT BODY -->
		<div class="page-content">
			<!-- BEGIN PAGE HEAD-->
			<div class="page-head">
				<!-- BEGIN PAGE TITLE -->
				<div class="page-title">
					<h1>
						评价列表
						<small>
							评价管理
						</small>
					</h1>
				</div>
				<!-- END PAGE TITLE -->
				<!-- BEGIN PAGE TOOLBAR -->
				<div class="page-toolbar">
				</div>
				<!-- END PAGE TOOLBAR -->
			</div>
			<!-- END PAGE HEAD-->
			<!-- BEGIN PAGE BASE CONTENT -->
			<div class="page-body portlet light">
				<%-- <div class="portlet-body">
					<form class="form-inline" role="form">
						<div class="form-group col-md-1">
							<button type="button" class="btn btn-default" style="border-color: #ffffff;">
								查询
							</button>
						</div>
						<div class="form-group col-md-offset-3 col-md-3">
							<select class="form-control" id="searchDepartId" name="searchDepartId">
								<option value="">
									请选择系别
								</option>
								<c:forEach items="${departList}" var="item">
									<option <c:if test="${param.searchDepartId eq item.departId}">
										selected="selected"
										</c:if>
										value="${item.departId}">${item.departTitle}
									</option>
								</c:forEach>
							</select>
						</div>
						<div class="form-group col-md-2">
							<button type="submit" class="btn btn-primary">
								确定
							</button>
						</div>
					</form>
			</div>
			--%>
			<div class="portlet-body">
				<table id="tableList" class="table">
				</table>
			</div>
		</div>
	</div>
	<!-- END CONTENT BODY -->
	</div>
	<script type="text/javascript">
		table();
		function openModal() {
			$('#myModal').modal('show');
		}
		function table() {
			$('#tableList').bootstrapTable('destroy');
			$('#tableList').bootstrapTable({
				url: '${ctx}/admin/loadOrdersList',
				queryParams: {},
				uniqueId: 'id',
				dataType: 'json',
				striped: true,
				pagination: true,
				pageNumber: 1,
				//初始化加载第一页，默认第一页
				pageSize: 10,
				//每页的记录行数（*）
				pageList: [10, 25, 50, 100],
				//可供选择的每页的行数（*）
				columns: [

				{
					field: 'number',
					title: '订单号',
					align: 'center',
				},
				{
					field: 'accountid',
					title: '购买者',
					align: 'center',
					formatter: function(value, row, index) {
						if (typeof(row.account) != 'undefined') {
							return row.account.accountNick;
						}
					}
				},
				{
					field: 'address',
					title: '地址',
					align: 'center',
				},
				{
					field: 'number',
					title: '订单号',
					align: 'center',
				},
				{
					field: 'time',
					title: '下单时间',
					align: 'center',
				},
				{
					field: 'count',
					title: '数量',
					align: 'center',
				},
				{
					field: 'address',
					title: '单价',
					align: 'center',
					formatter: function(value, row, index) {
						if (typeof(row.itemList) != 'undefined') {
							var html = '';
							$.each(row.itemList,
							function(idx, obj) {
								html += obj.title + '#数量：' + obj.count + '#总价￥：' + obj.total + '<br>';
							});
							return html;
						}
					}
				},
				{
					field: 'total',
					title: '总价',
					align: 'center',
				},
				{
					field: 'state',
					title: '状态',
					align: 'center',
					formatter: function(value, row, index) {
						if (value == 0) {
							return '未付款';
						}
						if (value == 1) {
							return '已付款';
						}
						if (value == 2) {
							return '已发货';
						}
						if (value == 3) {
							return '已收货';
						}
					}
				},

				{
					field: 'id',
					title: '操作',
					align: 'center',
					formatter: function(value, row, index) {
						var edit = ''; //'<a class="btn btn-xs btn-warning" onclick="edit(\''+value+'\');">修改</a>';
						var del = '';
						var send = '';
						if (row.state == 1) {
							edit = '<a class="btn btn-xs btn-warning" onclick="edit(\''+value+'\');">修改</a>';
							send = '<a class="btn btn-xs btn-info" onclick="pay(\'' + value + '\',2);">确认已发货</a>';
						}
						if (row.state == 0) {
							edit = '<a class="btn btn-xs btn-warning" onclick="edit(\''+value+'\');">修改</a>';
							del = '<button class="btn btn-xs btn-danger" onclick="remove(\'' + value + '\');">删除</button>';
						}
						return send + '&nbsp;&nbsp;' + edit + '&nbsp;&nbsp;' + del;
					}
				}],
			});
		}
		function edit(id) {
			window.location.href = '${ctx}/admin/diancanOrdersForm?diancanOrders.id=' + id;
		}
		function remove(id) {
			$.ajax({
				type: 'post',
				cache: false,
				url: '${ctx}/admin/deleteOrders',
				data: {
					'ordersId': id
				},
				dataType: 'json',
				success: function(res) {
					alert(res.data);
					table();
				},
				error: function() {
					alert('无法连接服务');
				}
			});
		}
		function pay(id, state) {
			$.ajax({
				type: 'post',
				cache: false,
				url: '${ctx}/admin/changeOrders',
				data: {
					'ordersId': id,
					'state': state
				},
				dataType: 'json',
				success: function(res) {
					alert(res.data);
					table();
				},
				error: function() {
					alert('无法连接服务');
				}
			});
		}
	</script>
</body>

</html>