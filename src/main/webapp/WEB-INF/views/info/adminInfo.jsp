<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/commons/tags.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>微信小店管理系统</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
    <div class="page-content-wrapper ">
	     <!-- BEGIN CONTENT BODY -->
	     <div class="page-content">
	         <!-- BEGIN PAGE HEAD-->
	         <div class="page-head">
	             <!-- BEGIN PAGE TITLE -->
	             <div class="page-title">
	                 <h1>个人中心
	                     <small>基本账号管理</small>
	                 </h1>
	             </div>
	             <!-- END PAGE TITLE -->
	             <!-- BEGIN PAGE TOOLBAR -->
	             <div class="page-toolbar">
	                 
	             </div>
	             <!-- END PAGE TOOLBAR -->
	         </div>
	         <!-- END PAGE HEAD-->
	         <!-- BEGIN PAGE BASE CONTENT -->
	         <div class="page-body portlet light">
	         <!-- BEGIN PAGE BASE CONTENT -->
              <div class="row">
                  <div class="col-md-12">
                      <!-- BEGIN PROFILE SIDEBAR -->
                      <%--
                      <div class="  col-xs-4">
                          <ul class="list-unstyled profile-nav">
                              <li>
                                  <img src="${accountInfo.accountLogo}" class="img-responsive pic-bordered" alt="" style="width:90%"/>
                                  <a href="javascript:;" class="profile-edit"> 编辑 </a>
                              </li>
                              <!-- <li>
                                  <a href="javascript:;"> 发布项目 </a>
                              </li>
                              <li>
                                  <a href="javascript:;"> 消息
                                      <span> 3 </span>
                                  </a>
                              </li>
                              <li>
                                  <a href="javascript:;"> 好友 </a>
                              </li>
                              <li>
                                  <a href="javascript:;"> 关注 </a>
                              </li> -->
                          </ul>
                      </div>
                      --%>
                      <!-- END BEGIN PROFILE SIDEBAR -->
                      <!-- BEGIN PROFILE CONTENT -->
                      <div class="profile-content col-xs-8">
                          <div class="row">
                              <div class="col-md-12">
                                  <div class="portlet light bordered">
                                      <div class="portlet-title tabbable-line">
                                          <div class="caption caption-md">
                                              <i class="icon-globe theme-font hide"></i>
                                              <span class="caption-subject font-blue-madison bold uppercase">账号信息</span>
                                          </div>
                                          <ul class="nav nav-tabs">
                                              <li class="active">
                                                  <a href="#tab_1_1" data-toggle="tab">个人信息</a>
                                              </li>
                                              <!-- <li>
                                                  <a href="#tab_1_2" data-toggle="tab">头像设置</a>
                                              </li> -->
                                              <li>
                                                  <a href="#tab_1_3" data-toggle="tab">修改密码</a>
                                              </li>
                                          </ul>
                                      </div>
                                      <div class="portlet-body">
                                          <div class="tab-content">
                                              <!-- PERSONAL INFO TAB -->
                                              <div class="tab-pane active" id="tab_1_1">
                                                  <form role="form" id="updateAccountForm" action="${ctx}/account/updateAccount" method="post">
		                                        	<div class="form-body">
		                                            <div class="form-group">
		                                                <label class="control-label">用户名<span class="required"> * </span></label>
		                                                <input type="text" placeholder="用户名" class="form-control" name="username" value="${admin.adminName}" readonly /> 
		                                            </div>
		                                            <div class="form-group">
			                                            <label class="control-label">昵称</label>
			                                            <input type="text" placeholder="昵称" class="form-control" name="nickname" value="${admin.adminNickname}" /> 
			                                        </div>
		                                            <div class="margiv-top-10">
		                                                <button type="submit" class="btn green"> 保存 </button>
		                                                <button type="reset" class="btn default"> 取消 </button>
		                                            </div>
		                                        	</div>
		                                        </form>
                                              </div>
                                              <!-- END PERSONAL INFO TAB -->
                                              <!-- CHANGE AVATAR TAB -->
                                              <div class="tab-pane" id="tab_1_2">
                                                  	<p> 头像大小说明</p>
			                                        <form action="#" role="form">
			                                            <div class="form-group">
			                                                <div class="fileinput fileinput-new" data-provides="fileinput">
			                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
			                                                        <img src="${accountInfo.accountLogo}" alt="" /> </div>
			                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
			                                                    <div>
			                                                        <span class="btn default btn-file">
			                                                            <span class="fileinput-new"> 选择头像 </span>
			                                                            <span class="fileinput-exists"> 修改 </span>
			                                                            <input type="file" name="file" id="file_logo"> </span>
			                                                        <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> 移除 </a>
			                                                    </div>
			                                                </div>
			                                                <div class="clearfix margin-top-10">
			                                                    <span class="label label-danger"> 注意! </span>
			                                                    <span> 注意。。。</span>
			                                                </div>
			                                            </div>
			                                            <div class="margin-top-10">
			                                                <a href="javascript:;" class="btn green" onclick="fileInput();"> 提交 </a>
			                                                <a href="javascript:;" class="btn default"> 取消 </a>
			                                            </div>
			                                        </form>
                                              </div>
                                              <!-- END CHANGE AVATAR TAB -->
                                              <!-- CHANGE PASSWORD TAB -->
                                              <div class="tab-pane" id="tab_1_3">
                                                  <form id="updatePasswordForm">
		                                            <div class="form-group">
		                                                <label class="control-label">原密码</label>
		                                                <input type="password" name="password" class="form-control" /> </div>
		                                            <div class="form-group">
		                                                <label class="control-label">新密码</label>
		                                                <input type="password" name="password1" id="password1" class="form-control" /> </div>
		                                            <div class="form-group">
		                                                <label class="control-label">重复密码</label>
		                                                <input type="password" name="password2" id="password2" class="form-control" /> </div>
		                                            <div class="margin-top-10">
		                                                <button type="submit" class="btn green"> 修改 </button>
		                                                <button type="reset" class="btn default"> 取消 </button>
		                                            </div>
		                                        </form>
                                              </div>
                                              <!-- END CHANGE PASSWORD TAB -->
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                      <!-- END PROFILE CONTENT -->
                  </div>
              </div>
              <!-- END PAGE BASE CONTENT -->
	         </div>
	     </div>
	     <!-- END CONTENT BODY -->
	 </div>
	 <script type="text/javascript">
	 $('#age').datepicker({
         rtl: App.isRTL(),
         orientation: "left",
         autoclose: true,
         language: "zh-CN",
         format: 'yyyy-mm-dd',
     });
	 $(function(){
         var form1 = $('#updateAccountForm');
         var error1 = $('.alert-danger', form1);
         var success1 = $('.alert-success', form1);
         var updateAccountForm = form1.validate({
             errorElement: 'span', //default input error message container
             errorClass: 'help-block help-block-error', // default input error message class
             focusInvalid: false, // do not focus the last invalid input
             ignore: ".ignore",  // validate all fields including form hidden input
             messages: {
             	nickname:{
             		required:'请输入',
                 },
                 email:{
                 	required:'请输入',
                 },
                 mobile:{
                 	required:'请输入',
                 },
                 sex:{
                 	required:'请输入',
                 },
                 age:{
                 	required:'请输入',
                 },
             },
             rules: {
             	nickname:{
             		minlength: 2,
                     required:true
                 },
                 email:{
                     //required:true,
                     email:true
                 },
                 mobile:{
                     //required:true,
                     //number:true
                 },
                 sex:{
                     required:true,
                     mobile:true
                 },
                 age:{
                     required:true,
                     //digits:true
                 },
             },
				/*
             invalidHandler: function (event, validator) { //display error alert on form submit              
                 success1.hide();
                 error1.show();
                 App.scrollTo(error1, -200);
             },
             errorPlacement: function (error, element) { // render error placement for each input type
                 var cont = $(element).parent('.input-group');
                 if (cont) {
                     cont.after(error);
                 } else {
                     element.after(error);
                 }
             },
             */
             highlight: function (element) { // hightlight error inputs
                 $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
             },
             unhighlight: function (element) { // revert the change done by hightlight
                 $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
             },
             success: function (label) {
                 label.closest('.form-group').removeClass('has-error'); // set success class to the control group
             },
             submitHandler: function (form) {
             	var flag = $("#updateAccountForm").valid();
             	$.ajax({
                     type: "post",
                     cache: false,
                     url: '${ctx}/admin/adminName',
                     data:$('#updateAccountForm').serialize(),
                     dataType: 'json',
                     success: function (res) {    
                         swal(res.data, '', res.msg);
                     },
                     error: function (res, ajaxOptions, thrownError) {
                     	alert('<h4>页面无法加载...</h4>');
                     }
                 });
             }
         });
 });

	$(function(){
     var form1 = $('#updatePasswordForm');
     var error1 = $('.alert-danger', form1);
     var success1 = $('.alert-success', form1);
     var updateAccountForm = form1.validate({
         errorElement: 'span', //default input error message container
         errorClass: 'help-block help-block-error', // default input error message class
         focusInvalid: false, // do not focus the last invalid input
         ignore: ".ignore",  // validate all fields including form hidden input
         messages: {
         	password: {  
        			required:'请输入',  
                 minlength: '最少输入六位号码'  
                },
             password1: {  
		   			required:'请输入',  
		            minlength: '最少输入六位号码',
		            equalTo: '请输入相同的密码'
		           },
             password2: {  
		   			required:'请输入',  
		            minlength: '最少输入六位号码',
		            equalTo: '请输入相同的密码'
		           }
         },
         rules: {
         	password:{
         		minlength: 6,
                 required:true
             },
             password1:{
         		minlength: 6,
                 required:true,
                 equalTo: "#password2"
             },
             password2:{
         		minlength: 6,
                 required:true,
                 equalTo: "#password1"
             },
         },
         highlight: function (element) { // hightlight error inputs
             $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
         },
         unhighlight: function (element) { // revert the change done by hightlight
             $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
         },
         success: function (label) {
             label.closest('.form-group').removeClass('has-error'); // set success class to the control group
         },
         submitHandler: function (form) {
         	var flag = $("#updatePasswordForm").valid();
         	$.ajax({
                 type: "post",
                 cache: false,
                 url: '${ctx}/admin/adminPsw',
                 data:$('#updatePasswordForm').serialize(),
                 dataType: 'json',
                 success: function (res) {    
                     swal(res.data, '', res.msg);
                 },
                 error: function (res, ajaxOptions, thrownError) {
                 	pageContent.html('<h4>页面无法加载...</h4>');
                 }
             });
         }
     });
	});
	/* function fileInput() {
 	var idDom = $('#file_logo');
 	var formData = new FormData();
 	var file = idDom.val();
 	formData.append('file',idDom[0].files[0]);
 	formData.append('name',file);
 	$.ajax({
         type: 'post',
         cache: false,
         url: '${ctx}/file/uploadFile.do',
         data:formData,
         dataType: 'json',
         processData : false,
         contentType : false,
         beforeSend:function(){
         	console.log('正在进行，请稍候');
         },
         success: function (res) {
         	if(res.status){
         		fileUploadForProject(res.viewPath);
         		swal(res.msg, '', 'success');
         	}
         	if(!res.status){
         		swal(res.msg, '', 'warning');
         	}
         },
         error: function (res, ajaxOptions, thrownError) {
         	console.log('无法连接服务');
         }
     });
	 } */
	 function fileUploadForProject(path) {
	 	$.ajax({
	         type: 'post',
	         cache: false,
	         url: '${ctx}/account/setHead.do',
	         data: {path:path},
	         dataType: 'json',
	         success: function (res) {
	         	swal(res.msg, '', 'warning');
	         },
	     });
	 }
	 </script>
  </body>
</html>
