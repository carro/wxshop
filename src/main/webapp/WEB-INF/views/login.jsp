<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/commons/tags.jsp" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>微信小店后台管理系统</title>
        <%@ include file="/WEB-INF/views/commons/css&js.jsp" %> 
        <style type="text/css">
        .login .content .form-control {
    background-color: #fdfdfd;
    height: 43px;
    color: #8290a3;
    border: 1px solid #dde3ec;
}
        </style>
        </head>
    <!-- END HEAD -->

    <body class="login">
    <div class="clearfix" style="margin-top:150px;"> </div>
        <!-- BEGIN LOGIN -->
        <div class="content">
            <div class="row">
         		<div class="col-md-12">
         			<!-- BEGIN FORM-->
		             <form class="login-form form-horizontal" id="loginForm" action="${ctx}/logon" method="post">
	                     <div class="form-group">
	                         <div class="col-md-12">
	                         	<br><br>
	                             <h3>微信小店后台管理系统</h3>
	                         </div>
	                     </div>
	                     <div class="form-group">
	                         <div class="col-md-12">
	                             <div class="alert alert-danger display-hide">
				                    <button class="close" data-close="alert"></button>
				                    <span>请输入用户名或密码 </span>
				                </div>
				                <c:if test="${not empty msg}">
				                <div class="alert alert-warning">
				                    <button class="close" data-close="alert"></button>
				                    <span>${msg} </span>
				                </div>
				                </c:if>
	                         </div>
	                     </div>
	                     <div class="form-group">
	                         <label class="control-label col-md-3">账号
	                             <span class="required"> * </span>
	                         </label>
	                         <div class="col-md-9">
	                             <input type="text" name="username" class="form-control" placeholder="工号/账号" value="admin" />
	                         </div>
	                     </div>
	                     <div class="form-group">
	                         <label class="control-label col-md-3">密码
	                             <span class="required"> * </span>
	                         </label>
	                         <div class="col-md-9">
	                             <input type="password" name="password" class="form-control" value="123456" />
	                         </div>
	                     </div>
	                     <div class="form-group ">
	                     	<div class="col-md-offset-3 col-md-4">
	                     		
	                     	</div>
	                     	<div class="col-md-5">
	                     		<button type="submit" class="form-control btn green uppercase btn-block">登录</button>
	                     	</div>
	                     </div>
	             </form>
	             <!-- END FORM-->
        		</div>
        </div>
     </div>
    </body>
</html>