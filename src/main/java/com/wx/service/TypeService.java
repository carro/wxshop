package com.wx.service;

import java.util.List;

import com.wx.entity.GoodsType;

public interface TypeService {

	public GoodsType getById(String studentId);
	
	public boolean save(GoodsType student);
	
	public boolean update(GoodsType student);
	
	public List<GoodsType> getList();
	
	public boolean remove(String objId);
}
