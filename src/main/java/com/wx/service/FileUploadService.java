package com.wx.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

public interface FileUploadService {
	/**
	 * 
	 * @Description: <p>文件上传</p>
	 * @author ChenJiaLu
	 * @param @param file
	 * @param @param request
	 * @param @param response
	 * @param @return   
	 * @return Map<String,Object>
	 */
	Map<String, Object> upload(MultipartFile file , HttpServletRequest request , HttpServletResponse response);
	/**
	 * 
	 * @Description: <p>上传保存Base64数据流的图片文件</p>
	 * @author ChenJiaLu
	 * @date 2016-12-7
	 * @param @param data   图片数据流
	 * @param @param request
	 * @param @return   
	 * @return Map<String,Object>
	 */
	Map<String, Object> Base64ToImage(String data , String logoState, HttpServletRequest request, HttpServletResponse response);
}
