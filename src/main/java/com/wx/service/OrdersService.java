package com.wx.service;

import java.util.List;

import com.wx.entity.Orders;
import com.wx.entity.OrdersTj;
   
public interface OrdersService{
		public Orders loadOrders(String id);
	public List<Orders> loadOrdersList();

	public List<Orders> loadOrdersListByAccountid(String accountid);

	public List<Orders> loadOrdersByDateWidthAccountid(String date,String accountid);

	public List<Orders> loadOrdersByDate(String date);

	public List<Orders> loadOrdersGroupByProduct();

	public List<Orders> loadOrdersByProduct(String productid);
	public List<Orders> loadOrdersGroupByAccount();

	public List<Orders> loadOrdersByAccount(String accountid);
		public boolean saveOrUpdateOrders(Orders diancanOrders);
	public boolean deleteOrders(String id);
	
	List<OrdersTj> getNearBy7Day();
}

