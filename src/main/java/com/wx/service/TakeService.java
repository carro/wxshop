package com.wx.service;

import java.util.List;

import com.wx.entity.Take;

public interface TakeService {

	public Take getById(String commonId);
	
	public boolean save(Take taks);
	
	public boolean update(Take taks);
	
	public List<Take> getList(Take taks);
	
	public boolean remove(String objId);
}
