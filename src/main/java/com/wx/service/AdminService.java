package com.wx.service;

import com.wx.entity.Admin;

public interface AdminService {

	public Admin getAdminById(String id);
	
	public Admin getAdminByName(String name,String password);
	
	public boolean updateAdminName(String adminId,String adminName);
	
	public int updateAdminPassword(String adminId,String newPassword,String oldPassword);
}
