package com.wx.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wx.dao.TakeMapper;
import com.wx.entity.Take;
import com.wx.service.TakeService;

@Service("TakeService")
public class TakeServiceImpl implements TakeService {

	@Autowired
	private TakeMapper takeMapper;
	
	@Override
	public Take getById(String classesId) {
		Take take = takeMapper.selectByPrimaryKey(classesId);
		return take;
	}

	@Override
	public boolean save(Take take) {
		int row = takeMapper.insertSelective(take);
		if(row>0){
			return true;
		}
		return false;
	}

	@Override
	public boolean update(Take take) {
		int row = takeMapper.updateByPrimaryKeySelective(take);
		if(row>0){
			return true;
		}
		return false;
	}

	@Override
	public List<Take> getList(Take take) {
		List<Take> list = takeMapper.selectAll(take);
		return list;
	}

	@Override
	public boolean remove(String objId) {
		int row = takeMapper.deleteByPrimaryKey(objId);
		if(row>0){
			return true;
		}
		return false;
	}

}
