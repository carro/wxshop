package com.wx.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wx.dao.ProductMapper;
import com.wx.entity.Product;
import com.wx.service.ProductService;
@Service("ClassesService")
public class ProductServiceImp implements ProductService {

	@Autowired
	private ProductMapper productMapper;
	
	@Override
	public Product getById(String classesId) {
		Product product = productMapper.selectByPrimaryKey(classesId);
		return product;
	}

	@Override
	public boolean save(Product product) {
		int row = productMapper.insertSelective(product);
		if(row>0){
			return true;
		}
		return false;
	}

	@Override
	public boolean update(Product product) {
		int row = productMapper.updateByPrimaryKeySelective(product);
		if(row>0){
			return true;
		}
		return false;
	}

	@Override
	public List<Product> getList(Product product) {
		List<Product> list = productMapper.selectAll(product);
		return list;
	}

	@Override
	public boolean remove(String objId) {
		int row = productMapper.deleteByPrimaryKey(objId);
		if(row>0){
			return true;
		}
		return false;
	}

	@Override
	public List<Product> getHotList(String title) {
		if(title == null || title.equals("")) {
			return productMapper.selectHotAll();
		}
		Product product = new Product();
		product.setProductTitle(title);
		return getList(product);
	}

}
