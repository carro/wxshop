package com.wx.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wx.dao.AdminMapper;
import com.wx.entity.Admin;
import com.wx.service.AdminService;
import com.wx.utils.KLKJUtil;
import com.wx.utils.pay.MD5;
@Service("AdminService")
public class AdminServiceImpl implements AdminService {

	@Autowired
	private AdminMapper adminMapper;

	@Override
	public Admin getAdminById(String id) {
		try {
			Admin info = adminMapper.selectByPrimaryKey(id);
			return info;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public Admin getAdminByName(String name, String password) {
		try {
			Admin record = new Admin();
			record.setAdminName(name);
			record.setAdminPassword(password);
			Admin info = adminMapper.selectByPassword(record);
			return info;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean updateAdminName(String adminId, String adminName) {
		try {
			Admin admin = adminMapper.selectByPrimaryKey(adminId);
			admin.setAdminId(adminId);
			admin.setAdminNickname(adminName);
			int row = adminMapper.updateByPrimaryKeySelective(admin);
			if(row>0){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public int updateAdminPassword(String adminId, String newPassword,
			String oldPassword) {
		try {
			if(KLKJUtil.isEmpty(adminId) || KLKJUtil.isEmpty(newPassword) || KLKJUtil.isEmpty(oldPassword)){
				return -1;//参数不对
			}
			Admin admin = adminMapper.selectByPrimaryKey(adminId);
			String oldPasswordMd5 = MD5.MD5Encode(oldPassword);
			if(!admin.getAdminPassword().equals(oldPasswordMd5)){
				return 2;//原密码不对
			}
			String newPasswordMd5 = MD5.MD5Encode(newPassword);
			admin.setAdminPassword(newPasswordMd5);
			int row = adminMapper.updateByPrimaryKeySelective(admin);
			if(row>0){
				return 1;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

}
