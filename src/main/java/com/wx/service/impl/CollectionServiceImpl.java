package com.wx.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wx.dao.CollectionMapper;
import com.wx.entity.CollectionInfo;
import com.wx.service.CollectionService;

@Service("CollectionService")
public class CollectionServiceImpl implements CollectionService {

	@Autowired
	private CollectionMapper collectionMapper;
	
	@Override
	public CollectionInfo getById(String classesId) {
		CollectionInfo collection = collectionMapper.selectByPrimaryKey(classesId);
		return collection;
	}

	@Override
	public boolean saveCollection(CollectionInfo collection) {
		int row = collectionMapper.insertSelective(collection);
		if(row>0){
			return true;
		}
		return false;
	}

	@Override
	public boolean updateCollection(CollectionInfo collection) {
		int row = collectionMapper.updateByPrimaryKeySelective(collection);
		if(row>0){
			return true;
		}
		return false;
	}

	@Override
	public List<CollectionInfo> getCollectionList(CollectionInfo collection) {
		List<CollectionInfo> list = collectionMapper.selectAll(collection);
		return list;
	}

	@Override
	public boolean removeCollection(String objId) {
		int row = collectionMapper.deleteByPrimaryKey(objId);
		if(row>0){
			return true;
		}
		return false;
	}
}
