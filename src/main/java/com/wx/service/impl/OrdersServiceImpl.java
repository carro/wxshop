package com.wx.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wx.dao.OrdersMapper;
import com.wx.entity.Orders;
import com.wx.entity.OrdersTj;
import com.wx.service.AccountService;
import com.wx.service.ProductService;
import com.wx.service.OrdersService;
import com.wx.utils.KLKJUtil;


@Service("OrdersService")
public class OrdersServiceImpl implements OrdersService {

	@Autowired
	private AccountService accountService;
	@Autowired
	private ProductService diancanProductService;
	@Autowired
	private OrdersMapper ordersMapper;

	@Override
	public Orders loadOrders(String id) {
		try {
			Orders info = ordersMapper.selectByPrimaryKey(id);
			return info;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Orders> loadOrdersList() {
		try {
			List<Orders> list = ordersMapper.selectAll(null,null,null);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Orders> loadOrdersListByAccountid(String accountid) {
		try {
			List<Orders> list = ordersMapper.selectAll(accountid,null,null);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Orders> loadOrdersByDateWidthAccountid(String date, String accountid) {
		try {
			List<Orders> list = ordersMapper.selectAll(accountid,date,null);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public List<Orders> loadOrdersByDate(String date) {
		try {
			List<Orders> list = ordersMapper.selectAll(null,date,null);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Orders> loadOrdersGroupByProduct() {
		try {
			List<Orders> list = ordersMapper.selectGroupByFid();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Orders> loadOrdersByProduct(String productid) {
		try {
			List<Orders> list = ordersMapper.selectAll(null,null,productid);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Orders> loadOrdersGroupByAccount() {
		try {
			List<Orders> list = ordersMapper.selectGroupByAid();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<Orders> loadOrdersByAccount(String accountid) {
		try {
			List<Orders> list = ordersMapper.selectAll(accountid,null,null);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean saveOrUpdateOrders(Orders diancanOrders) {
		try {
			int isSave = 0;
			Orders info = ordersMapper.selectByPrimaryKey(diancanOrders.getId());
			if(KLKJUtil.isEmpty(info)) {
				isSave = ordersMapper.insertSelective(diancanOrders);
			}else {
				isSave = ordersMapper.updateByPrimaryKeySelective(diancanOrders);
			}
			if (isSave > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean deleteOrders(String id) {
		try {
			int isDelete = ordersMapper.deleteByPrimaryKey(id);
			if (isDelete > 0) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<OrdersTj> getNearBy7Day() {
		List<OrdersTj> list = ordersMapper.selectNearBy7Day();
		return list;
	}
}
