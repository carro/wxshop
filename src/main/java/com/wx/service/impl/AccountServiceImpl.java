package com.wx.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wx.dao.AccountMapper;
import com.wx.entity.Account;
import com.wx.service.AccountService;
import com.wx.utils.EmojiFilter;
import com.wx.utils.KLKJUtil;

@Service("AccountService")
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountMapper accountMapper;
	
	@Override
	public Account getAccountByOpenId(String openId) {
		try {
			return accountMapper.selectByOpenId(openId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public boolean saveAccountWxInfo(String accountId, String openId, String nickname, 
			String logo, Integer gender, String province,
			String city, String area) {
		int row = 0;
		try {
			Account account = accountMapper.selectByOpenId(openId);
			if(KLKJUtil.isEmpty(account)){
				account = new Account();
				account.setAccountId(accountId);
				account.setAccountOpenid(openId);
				account.setAccountNick(EmojiFilter.filterEmoji(nickname));
				account.setAccountLogo(logo);
				account.setAccountSex(gender);
				//account.setAccountProvince(province);
				//account.setAccountCity(city);
				//account.setAccountArea(area);
				account.setAccountTime(KLKJUtil.getDateTimeStr());
				account.setAccountActive(1);
				row = accountMapper.insert(account);
			}else{
				account.setAccountNick(EmojiFilter.filterEmoji(nickname));
				account.setAccountLogo(logo);
				account.setAccountSex(gender);
				//account.setAccountProvince(province);
				//account.setAccountCity(city);
				//account.setAccountArea(area);
				row = accountMapper.updateByPrimaryKeySelective(account);
			}
			if(row>0){
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	@Override
	public Account getById(String classesId) {
		Account account = accountMapper.selectByPrimaryKey(classesId);
		return account;
	}

	@Override
	public boolean save(Account account) {
		int row = accountMapper.insertSelective(account);
		if(row>0){
			return true;
		}
		return false;
	}

	@Override
	public boolean update(Account account) {
		int row = accountMapper.updateByPrimaryKeySelective(account);
		if(row>0){
			return true;
		}
		return false;
	}

	@Override
	public List<Account> getList(Account account) {
		List<Account> list = accountMapper.selectAll(account);
		return list;
	}

	@Override
	public boolean remove(String objId) {
		int row = accountMapper.deleteByPrimaryKey(objId);
		if(row>0){
			return true;
		}
		return false;
	}

}
