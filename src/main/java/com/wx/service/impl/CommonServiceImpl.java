package com.wx.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wx.dao.CommonMapper;
import com.wx.entity.Common;
import com.wx.service.CommonService;

@Service("CommonService")
public class CommonServiceImpl implements CommonService {

	@Autowired
	private CommonMapper commonMapper;
	
	@Override
	public Common getByOrdersId(String ordersId, String productId) {
		Common common = commonMapper.selectByOrdersId(ordersId, productId);
		return common;
	}

	@Override
	public boolean save(Common common) {
		int row = commonMapper.insertSelective(common);
		if(row>0){
			return true;
		}
		return false;
	}

	/*@Override
	public boolean update(Common common) {
		int row = commonMapper.updateByPrimaryKeySelective(common);
		if(row>0){
			return true;
		}
		return false;
	}*/

	@Override
	public List<Common> getList(Common common) {
		List<Common> list = commonMapper.selectAll(common);
		return list;
	}

	@Override
	public boolean remove(String commonId) {
		int row = commonMapper.deleteByPrimaryKey(commonId);
		if(row>0){
			return true;
		}
		return false;
	}

}
