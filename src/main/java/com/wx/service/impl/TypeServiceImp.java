package com.wx.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wx.dao.GoodsTypeMapper;
import com.wx.entity.GoodsType;
import com.wx.service.TypeService;
@Service("DepartService")
public class TypeServiceImp implements TypeService {

	@Autowired
	private GoodsTypeMapper departMapper;
	
	@Override
	public GoodsType getById(String departId) {
		GoodsType depart = departMapper.selectByPrimaryKey(departId);
		return depart;
	}

	@Override
	public boolean save(GoodsType depart) {
		int row = departMapper.insertSelective(depart);
		if(row>0){
			return true;
		}
		return false;
	}

	@Override
	public boolean update(GoodsType depart) {
		int row = departMapper.updateByPrimaryKeySelective(depart);
		if(row>0){
			return true;
		}
		return false;
	}

	@Override
	public List<GoodsType> getList() {
		List<GoodsType> list = departMapper.selectAll();
		return list;
	}

	@Override
	public boolean remove(String objId) {
		int row = departMapper.deleteByPrimaryKey(objId);
		if(row>0){
			return true;
		}
		return false;
	}

}
