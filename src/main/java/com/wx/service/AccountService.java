package com.wx.service;

import java.util.List;

import com.wx.entity.Account;

public interface AccountService {

	public Account getById(String accountId);
	
	public boolean save(Account account);
	
	public boolean update(Account account);
	
	public List<Account> getList(Account account);
	
	public boolean remove(String objId);

	public boolean saveAccountWxInfo(String accountId, String openId, String nickname, String logo, Integer gender, String province,
			String city, String area);

	public Account getAccountByOpenId(String openId);
}
