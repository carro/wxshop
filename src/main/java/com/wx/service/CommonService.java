package com.wx.service;

import java.util.List;

import com.wx.entity.Common;

public interface CommonService {

	public Common getByOrdersId(String ordersId, String productId);
	
	public boolean save(Common common);
	
	//public boolean update(Common common);
	
	public List<Common> getList(Common common);
	
	public boolean remove(String commonId);
}
