package com.wx.service;

import java.util.List;

import com.wx.entity.CollectionInfo;

public interface CollectionService {public CollectionInfo getById(String collectionId);

public boolean saveCollection(CollectionInfo collection);

public boolean updateCollection(CollectionInfo collection);

public List<CollectionInfo> getCollectionList(CollectionInfo collection);

public boolean removeCollection(String objId);}
