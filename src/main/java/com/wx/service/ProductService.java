package com.wx.service;

import java.util.List;

import com.wx.entity.Product;

public interface ProductService {

	public Product getById(String studentId);
	
	public boolean save(Product student);
	
	public boolean update(Product student);
	
	public List<Product> getList(Product product);
	
	public boolean remove(String objId);
	
	public List<Product> getHotList(String title);
}
