package com.wx.entity;

public class Common {
    private String commonId;

    private String commonAccountId;
    
    private String accountNick;
    
    private String accountLogo;

    private String commonOrdersId;

    private String commonProductId;
    
    private String productTitle;

    private String commonContent;

    private String commonReply;

    private String commonTime;

    public String getCommonId() {
        return commonId;
    }

    public void setCommonId(String commonId) {
        this.commonId = commonId == null ? null : commonId.trim();
    }

    public String getCommonAccountId() {
        return commonAccountId;
    }

    public void setCommonAccountId(String commonAccountId) {
        this.commonAccountId = commonAccountId == null ? null : commonAccountId.trim();
    }

    public String getCommonProductId() {
        return commonProductId;
    }

    public void setCommonProductId(String commonProductId) {
        this.commonProductId = commonProductId == null ? null : commonProductId.trim();
    }

    public String getCommonContent() {
        return commonContent;
    }

    public void setCommonContent(String commonContent) {
        this.commonContent = commonContent == null ? null : commonContent.trim();
    }

    public String getCommonTime() {
        return commonTime;
    }

    public void setCommonTime(String commonTime) {
        this.commonTime = commonTime == null ? null : commonTime.trim();
    }

	public String getAccountNick() {
		return accountNick;
	}

	public void setAccountNick(String accountNick) {
		this.accountNick = accountNick;
	}

	public String getAccountLogo() {
		return accountLogo;
	}

	public void setAccountLogo(String accountLogo) {
		this.accountLogo = accountLogo;
	}

	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}

	public String getCommonOrdersId() {
		return commonOrdersId;
	}

	public void setCommonOrdersId(String commonOrdersId) {
		this.commonOrdersId = commonOrdersId;
	}

	public String getCommonReply() {
		return commonReply;
	}

	public void setCommonReply(String commonReply) {
		this.commonReply = commonReply;
	}
    
}