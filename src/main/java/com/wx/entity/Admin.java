package com.wx.entity;

import java.util.Date;

public class Admin {
    private String adminId;

    private String adminName;

    private String adminNickname;

    private String adminPassword;

    private Date adminLoginTime;

    
    public Admin() {
		super();
	}

	public Admin(String adminId, String adminName, String adminNickname,
			String adminPassword, Date adminLoginTime) {
		super();
		this.adminId = adminId;
		this.adminName = adminName;
		this.adminNickname = adminNickname;
		this.adminPassword = adminPassword;
		this.adminLoginTime = adminLoginTime;
	}

	public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId == null ? null : adminId.trim();
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName == null ? null : adminName.trim();
    }

    public String getAdminNickname() {
        return adminNickname;
    }

    public void setAdminNickname(String adminNickname) {
        this.adminNickname = adminNickname == null ? null : adminNickname.trim();
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword == null ? null : adminPassword.trim();
    }

    public Date getAdminLoginTime() {
        return adminLoginTime;
    }

    public void setAdminLoginTime(Date adminLoginTime) {
        this.adminLoginTime = adminLoginTime;
    }
}