package com.wx.entity;

import java.math.BigDecimal;

public class Item {
	private String productid;
	
	private String ordersid;
	
	private String title;
	
	private int count;
	
	private BigDecimal total;
    
    private int hasCommon = 0;

	public String getProductid() {
		return productid;
	}

	public void setProductid(String productid) {
		this.productid = productid;
	}

	public String getOrdersid() {
		return ordersid;
	}

	public void setOrdersid(String ordersid) {
		this.ordersid = ordersid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public int getHasCommon() {
		return hasCommon;
	}

	public void setHasCommon(int hasCommon) {
		this.hasCommon = hasCommon;
	}
	
}
