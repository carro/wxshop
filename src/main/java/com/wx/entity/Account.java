package com.wx.entity;

public class Account {
    private String accountId;

    private String accountNick;

    private String accountLogo;

    private Integer accountSex;

    private String accountOpenid;

    private Integer accountActive;

    private String accountTime;

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId == null ? null : accountId.trim();
    }

    public String getAccountNick() {
        return accountNick;
    }

    public void setAccountNick(String accountNick) {
        this.accountNick = accountNick == null ? null : accountNick.trim();
    }

    public String getAccountLogo() {
        return accountLogo;
    }

    public void setAccountLogo(String accountLogo) {
        this.accountLogo = accountLogo == null ? null : accountLogo.trim();
    }

    public Integer getAccountSex() {
        return accountSex;
    }

    public void setAccountSex(Integer accountSex) {
        this.accountSex = accountSex;
    }

    public String getAccountOpenid() {
        return accountOpenid;
    }

    public void setAccountOpenid(String accountOpenid) {
        this.accountOpenid = accountOpenid == null ? null : accountOpenid.trim();
    }

    public Integer getAccountActive() {
        return accountActive;
    }

    public void setAccountActive(Integer accountActive) {
        this.accountActive = accountActive;
    }

    public String getAccountTime() {
        return accountTime;
    }

    public void setAccountTime(String accountTime) {
        this.accountTime = accountTime == null ? null : accountTime.trim();
    }
}