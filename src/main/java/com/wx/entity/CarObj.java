package com.wx.entity;

import java.math.BigDecimal;

public class CarObj {
	
	private String id;
	
	private String title;
	
	private String image;
	
	private BigDecimal price;
	
	private int count;
	
	private BigDecimal total;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public BigDecimal getTotal() {
		return new BigDecimal(this.getCount()).multiply(this.getPrice());
	}

	public void setTotal(BigDecimal total) {
		this.total = new BigDecimal(this.getCount()).multiply(this.getPrice());
	}

	@Override
	public String toString() {
		return "CarObj [id=" + id + ", title=" + title + ", image=" + image
				+ ", price=" + price + ", count=" + count + ", total=" + total
				+ "]";
	}
	
	
	
}
