package com.wx.entity;

import com.wx.utils.KLKJUtil;

public class GoodsType {
	
    private String typeId;

    private String typeTitle;
    
    private String typeTime;

    private int active = 0;
    
    public GoodsType() {
		super();
	}

	public GoodsType(String typeId, String typeTitle) {
		super();
		this.typeId = typeId;
		this.typeTitle = typeTitle;
		this.typeTime = KLKJUtil.getDateTimeStr();
	}

	public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId == null ? null : typeId.trim();
    }

    public String getTypeTitle() {
        return typeTitle;
    }

    public void setTypeTitle(String typeTitle) {
        this.typeTitle = typeTitle == null ? null : typeTitle.trim();
    }

	public String getTypeTime() {
		return typeTime;
	}

	public void setTypeTime(String typeTime) {
		this.typeTime = typeTime;
	}
    
}