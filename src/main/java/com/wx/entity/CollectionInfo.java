package com.wx.entity;

public class CollectionInfo {
    private String collectionId;

    private String collectionAccountId;
    
    private String accountNick;
    
    private String accountLogo;

    private String collectionProductId;

    private String productTitle;

    private String collectionTime;

    public String getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(String collectionId) {
        this.collectionId = collectionId == null ? null : collectionId.trim();
    }

    public String getCollectionAccountId() {
        return collectionAccountId;
    }

    public void setCollectionAccountId(String collectionAccountId) {
        this.collectionAccountId = collectionAccountId == null ? null : collectionAccountId.trim();
    }


    public String getCollectionProductId() {
		return collectionProductId;
	}

	public void setCollectionProductId(String collectionProductId) {
		this.collectionProductId = collectionProductId;
	}

	public String getCollectionTime() {
        return collectionTime;
    }

    public void setCollectionTime(String collectionTime) {
        this.collectionTime = collectionTime == null ? null : collectionTime.trim();
    }

	public String getAccountNick() {
		return accountNick;
	}

	public void setAccountNick(String accountNick) {
		this.accountNick = accountNick;
	}

	public String getAccountLogo() {
		return accountLogo;
	}

	public void setAccountLogo(String accountLogo) {
		this.accountLogo = accountLogo;
	}

	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}
    
}