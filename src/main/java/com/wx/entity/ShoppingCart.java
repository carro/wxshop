package com.wx.entity;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import com.wx.utils.KLKJUtil;

public class ShoppingCart {
	private Map<String, CarObj> map = new LinkedHashMap<String, CarObj>();  
	private static ShoppingCart shoppingCart;
	BigDecimal total= new BigDecimal(0);
	private ShoppingCart(){}
	//产生单例
	public static ShoppingCart getShoppingCart(){
		if(shoppingCart==null){
			shoppingCart = new ShoppingCart();
		}
		return shoppingCart;
	}
	
	public BigDecimal getTotal(){
		try {
			total = new BigDecimal(0);
			for (Map.Entry<String, CarObj> entry : map.entrySet()) { 
				  total = total.add(entry.getValue().getPrice().multiply(new BigDecimal(entry.getValue().getCount())));
				}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return total;
	}
	
	public Map<String, CarObj> getCar(){
		try {
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean addCar(String id,CarObj obj){
		try {
			CarObj obj2 = map.get(id);
			if(KLKJUtil.isNotEmpty(obj2)){
				obj.setCount(obj2.getCount()+1);
			}
			map.put(id, obj);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean editCar(String id,CarObj obj){
		try {
			map.put(id, obj);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean removeCar(String id){
		try {
			map.remove(id);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public void clear(){
		map = new LinkedHashMap<String, CarObj>();  
	}
	
	public Map<String, CarObj> getMap() {
		return map;
	}
	public void setMap(Map<String, CarObj> map) {
		this.map = map;
	}
	
	
}
