package com.wx.entity;

public class BaseResponse<T> {
	int state;

	String msg;

	T data;

	public BaseResponse() {
	}

	public BaseResponse(int state, String msg, T data) {
		this.state = state;
		this.msg = msg;
		this.data = data;
	}

	public BaseResponse(int state, String msg) {
		this.state = state;
		this.msg = msg;
	} 

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
