package com.wx.entity;

import java.math.BigDecimal;
import java.util.List;

public class Product {
    private String productId;

    private String productTypeId;

    private String productTitle;

    private String productLogo;

    private String productCode;

    private String productBrand;

    private BigDecimal productPrice;

    private String productPublish;

    private String productSubtitle;

    private String productContent;

    private String productTime;
    
    private Integer productHot;

    private GoodsType type;
    
    private List<Common> commentList;
    
    public Product() {
		super();
	}

	public Product(String productId, String productTypeId, String productTitle, String productLogo, String productCode,
			String productBrand,
			BigDecimal productPrice, String productPublish, String productSubtitle, String productContent,
			String productTime,Integer productHot) {
		super();
		this.productId = productId;
		this.productTypeId = productTypeId;
		this.productTitle = productTitle;
		this.productLogo = productLogo;
		this.productCode = productCode;
		this.productBrand = productBrand;
		this.productPrice = productPrice;
		this.productPublish = productPublish;
		this.productSubtitle = productSubtitle;
		this.productContent = productContent;
		this.productTime = productTime;
		this.productHot = productHot;
	}

	public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId == null ? null : productId.trim();
    }

    public String getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(String productTypeId) {
        this.productTypeId = productTypeId == null ? null : productTypeId.trim();
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle == null ? null : productTitle.trim();
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode == null ? null : productCode.trim();
    }


    public BigDecimal getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(BigDecimal productPrice) {
		this.productPrice = productPrice;
	}

	public String getProductPublish() {
        return productPublish;
    }

    public void setProductPublish(String productPublish) {
        this.productPublish = productPublish == null ? null : productPublish.trim();
    }

    public String getProductSubtitle() {
        return productSubtitle;
    }

    public void setProductSubtitle(String productSubtitle) {
        this.productSubtitle = productSubtitle == null ? null : productSubtitle.trim();
    }

    public String getProductContent() {
        return productContent;
    }

    public void setProductContent(String productContent) {
        this.productContent = productContent == null ? null : productContent.trim();
    }

    public String getProductTime() {
        return productTime;
    }

    public void setProductTime(String productTime) {
        this.productTime = productTime == null ? null : productTime.trim();
    }

	public GoodsType getType() {
		return type;
	}

	public void setType(GoodsType type) {
		this.type = type;
	}

	public String getProductLogo() {
		return productLogo;
	}

	public void setProductLogo(String productLogo) {
		this.productLogo = productLogo;
	}

	public Integer getProductHot() {
		return productHot;
	}

	public void setProductHot(Integer productHot) {
		this.productHot = productHot;
	}

	public String getProductBrand() {
		return productBrand;
	}

	public void setProductBrand(String productBrand) {
		this.productBrand = productBrand;
	}

	public List<Common> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<Common> commentList) {
		this.commentList = commentList;
	}
    
}