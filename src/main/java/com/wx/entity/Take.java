package com.wx.entity;

public class Take {
    private String takeId;

    private String takeAccountId;
    
    private String accountNick;
    
    private String accountLogo;

    private String takeContent;

    private String takeTime;

    public String getTakeId() {
        return takeId;
    }

    public void setTakeId(String takeId) {
        this.takeId = takeId == null ? null : takeId.trim();
    }

    public String getTakeAccountId() {
        return takeAccountId;
    }

    public void setTakeAccountId(String takeAccountId) {
        this.takeAccountId = takeAccountId == null ? null : takeAccountId.trim();
    }

    public String getTakeContent() {
        return takeContent;
    }

    public void setTakeContent(String takeContent) {
        this.takeContent = takeContent == null ? null : takeContent.trim();
    }

    public String getTakeTime() {
        return takeTime;
    }

    public void setTakeTime(String takeTime) {
        this.takeTime = takeTime == null ? null : takeTime.trim();
    }

	public String getAccountNick() {
		return accountNick;
	}

	public void setAccountNick(String accountNick) {
		this.accountNick = accountNick;
	}

	public String getAccountLogo() {
		return accountLogo;
	}

	public void setAccountLogo(String accountLogo) {
		this.accountLogo = accountLogo;
	}
    
}