package com.wx.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.reflect.TypeToken;
import com.wx.entity.Account;
import com.wx.entity.BaseResponse;
import com.wx.entity.CarObj;
import com.wx.entity.Product;
import com.wx.entity.CollectionInfo;
import com.wx.entity.Common;
import com.wx.entity.GoodsType;
import com.wx.entity.Item;
import com.wx.entity.Orders;
import com.wx.entity.ShoppingCart;
import com.wx.entity.Take;
import com.wx.service.AccountService;
import com.wx.service.ProductService;
import com.wx.service.CollectionService;
import com.wx.service.CommonService;
import com.wx.service.TypeService;
import com.wx.service.OrdersService;
import com.wx.service.TakeService;
import com.wx.utils.AesCbcUtil;
import com.wx.utils.EmojiFilter;
import com.wx.utils.KLKJID;
import com.wx.utils.KLKJJsonUtil;
import com.wx.utils.KLKJUtil;
import com.wx.utils.pay.Configure;
import com.wx.utils.pay.HttpRequest;


@Controller
@RequestMapping("/api")
public class ApiController {
	@Autowired
	private ProductService productService;
	@Autowired
	private TypeService typeService;
	@Autowired
	private AccountService accountService;
	@Autowired
	private CommonService commonService;
	@Autowired
	private TakeService takeService;
	@Autowired
	private OrdersService ordersService;
	@Autowired
	private CollectionService collectionService;
	
	/*@Autowired
	RestTemplate restTemplate;
	
	@Bean
	public RestTemplate restTemplate(ClientHttpRequestFactory factory) {
	    return new RestTemplate(factory);
	}*/
	
	@RequestMapping("/hotList")
	@ResponseBody
    public List<Product> hotList(
    		@RequestParam(value="title",required=false, defaultValue = "")String title){
		return productService.getHotList(title);
	}
	
	@RequestMapping("/indexList")
	@ResponseBody
    public Map<String,Object> indexList(
    		@RequestParam(value="typeId",required=false)String typeId,
    		@RequestParam(value="title",required=false)String title){
		Map<String,Object> resMap = new HashMap<String,Object>();
		resMap.put("typeList", new ArrayList<GoodsType>());
		resMap.put("productList", new ArrayList<Product>());
		try {
			List<GoodsType> list = typeService.getList();
			resMap.put("typeList", list);
			Product search = new Product();
			search.setProductTypeId(typeId);
			search.setProductTitle(title);
			List<Product> list2 = productService.getList(search);
			resMap.put("productList", list2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resMap;
	}
	
	@RequestMapping("/typeList")
	@ResponseBody
    public BaseResponse<List<GoodsType>> typeList(){
		try {
			List<GoodsType> list = new ArrayList<GoodsType>();
			GoodsType type = new GoodsType();
			type.setTypeTitle("所有");
			list.add(type);
			List<GoodsType> list2 = typeService.getList();
			if(KLKJUtil.isNotEmpty(list2)) {
				list.addAll(list2);
			}
			return new BaseResponse<List<GoodsType>>(0,"SUCCESS",list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<List<GoodsType>>(0,"ERROR",null);
	}
	
	@RequestMapping("/productList")
	@ResponseBody
    public BaseResponse<List<Product>> productList(
    		@RequestParam(value="typeId",required=false)String typeId,
    		@RequestParam(value="title",required=false)String title){
		Product product = new Product();
		try {
			product.setProductTypeId(KLKJUtil.isNotEmpty(typeId)?typeId:null);
			product.setProductTitle(KLKJUtil.isNotEmpty(title)?title:null);
			List<Product> list = productService.getList(product);
			return new BaseResponse<List<Product>>(0,"SUCCESS",list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<List<Product>>(0,"ERROR",null);
	}
	
	@RequestMapping("/productInfo")
	@ResponseBody
    public BaseResponse<Product> productInfo(
    		@RequestParam(value="productId",required=false)String productId){
		try {
			Product product = productService.getById(productId);
			Common common = new Common();
			common.setCommonProductId(productId);
			List<Common> list = commonService.getList(common);
			product.setCommentList(list);
			return new BaseResponse<Product>(0,"SUCCESS",product);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<Product>(0,"ERROR",null);
	}
	
	@ResponseBody
	@RequestMapping(value = "/apiLogin")
	public BaseResponse<Map<String,Object>> apiLogin(
			@RequestParam(value="code")String code,
			@RequestParam(value="encryptedData")String encryptedData,
			@RequestParam(value="iv")String iv) {
		if(KLKJUtil.isEmpty(code)){
			return new BaseResponse<Map<String,Object>>(0,"error",null);
		}
		String wxspAppid = Configure.getAppID();
		String wxspSecret = Configure.getAppSecret();
		String grant_type = Configure.getGrant_type();
		String params = "appid=" + wxspAppid + "&secret=" + wxspSecret+ "&js_code=" + code + "&grant_type=" + grant_type;
		try {
			//Object  obj=  restTemplate.getForObject("https://api.weixin.qq.com/sns/jscode2session?appid=*****&secret=*****&js_code=" +code + "&grant_type=authorization_code",String.class);
	        //String object1 = obj.toString();
			String result = HttpRequest.sendGet("https://api.weixin.qq.com/sns/jscode2session", params);
			Map<String, Object> json = KLKJJsonUtil.fromJson(result,new TypeToken<Map<String, Object>>() {}.getType());
			boolean flag=json.containsKey("openid");//openid   session_key
			if(flag){
				String openId = json.get("openid").toString();
				String sessionKey = json.get("session_key").toString();
				// String encryptData, String session_key, String iv, String encodingFormat
				String aescbResult = AesCbcUtil.decrypt(encryptedData, sessionKey, iv,"UTF-8");
				Map<String,Object> userInfo = KLKJJsonUtil.fromJson(aescbResult, new TypeToken<Map<String, Object>>() {}.getType());
				String accountId = KLKJID.uuid();
				String nickname = userInfo.get("nickName").toString();
				String logo = userInfo.get("avatarUrl").toString();
				String province = userInfo.get("province").toString();
				String city = userInfo.get("city").toString();
				String area = userInfo.get("country").toString();
				String gender = userInfo.get("gender").toString();
				Integer genders = 1;
				if(KLKJUtil.isNotEmpty(gender)) {
					genders = new BigDecimal(gender).intValue();
				}
				nickname = EmojiFilter.filterEmoji(nickname);
				nickname = EmojiFilter.filter(nickname);
				nickname = EmojiFilter.filterName(nickname);
				boolean saveAccount = accountService.saveAccountWxInfo(accountId, openId, nickname, 
						logo, genders, province, city, area);
				if(saveAccount){
					Account account = accountService.getAccountByOpenId(openId);
					if(account.getAccountActive()==1){
						userInfo.put("accountId", account.getAccountId());
						return new BaseResponse<Map<String,Object>>(1,"登录成功",userInfo);
					}
					if(account.getAccountActive()==0){
						return new BaseResponse<Map<String,Object>>(0,"账号被禁用",null);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new BaseResponse<Map<String,Object>>(0,"登录失败");
		}
		return new BaseResponse<Map<String,Object>>(0,"登录失败",null);
	}
	
	private String getOpenid(String code){
        //调用微信接口服务，获取当前用户的openid
        Map<String,String> map = new HashMap<>();
        /*微信官方固定参数*/
        map.put("appid", Configure.getAppID());
        map.put("secret", Configure.getAppSecret());
        map.put("js_code",code);
        map.put("grant_type","authorization_code");
        /*获取返回的id*/
        String json = HttpRequest.doGet("https://api.weixin.qq.com/sns/jscode2session", map);
        /*解析数据拿到openid*/
        JSONObject jsonObject = JSON.parseObject(json);
        String openid = jsonObject.getString("openid");

        return openid;
    }
	
	@RequestMapping("/accountInfo")
	@ResponseBody
    public BaseResponse<Account> accountInfo(
    		@RequestParam(value="accountId",required=false)String accountId){
		try {
			Account account = accountService.getById(accountId);
			return new BaseResponse<Account>(0,"SUCCESS",account);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<Account>(0,"ERROR",null);
	}
	
	@RequestMapping("/commonSave")
	@ResponseBody
    public BaseResponse<String> commonSave(
    		@RequestParam(value="accountId",required=false)String accountId,
    		@RequestParam(value="ordersId",required=false)String ordersId,
    		@RequestParam(value="productId",required=false)String productId,
    		@RequestParam(value="content",required=false)String commonContent){
		try {
			Account account = accountService.getById(accountId);
			if(KLKJUtil.isNotEmpty(account) && account.getAccountActive() == 0) {
				return new BaseResponse<String>(0,"黑名单无法操作");
			}
			Orders od = ordersService.loadOrders(ordersId);
			if(null == od) {
				return new BaseResponse<String>(0,"订单不存在");
			}
			Common common = new Common();
			common.setCommonId(KLKJID.uuid());
			common.setCommonOrdersId(ordersId);
			common.setCommonAccountId(accountId);
			common.setCommonProductId(productId);
			common.setCommonContent(commonContent);
			common.setCommonTime(KLKJUtil.getDateTimeStr());
			boolean res = commonService.save(common);
			if(res) {
				return new BaseResponse<String>(1,"评论成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<String>(0,"提交失败");
	}
	
	@RequestMapping("/commonList")
	@ResponseBody
    public BaseResponse<List<Common>> commonList(
    		@RequestParam(value="accountId",required=false)String accountId,
    		@RequestParam(value="productId",required=false)String productId){
		Common common = new Common();
		try {
			common.setCommonAccountId(accountId);
			common.setCommonProductId(productId);
			List<Common> list = commonService.getList(common);
			return new BaseResponse<List<Common>>(0,"SUCCESS",list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<List<Common>>(0,"ERROR",null);
	}
	
	@RequestMapping("/commonDel")
	@ResponseBody
    public BaseResponse<String> commonDel(
    		@RequestParam(value="commonId",required=false)String commonId){
		try {
			boolean result = commonService.remove(commonId);
			if(result) {
				return new BaseResponse<String>(1,"删除成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<String>(0,"删除失败");
	}
	
	@RequestMapping("/takeSave")
	@ResponseBody
    public BaseResponse<String> takeSave(
    		@RequestParam(value="accountId",required=false)String accountId,
    		@RequestParam(value="content",required=false)String content){
		try {
			Account account = accountService.getById(accountId);
			if(KLKJUtil.isNotEmpty(account) && account.getAccountActive() == 0) {
				return new BaseResponse<String>(0,"黑名单无法操作");
			}
			Take take = new Take();
			take.setTakeId(KLKJID.uuid());
			take.setTakeContent(content);
			take.setTakeAccountId(accountId);
			take.setTakeTime(KLKJUtil.getDateTimeStr());
			boolean result = takeService.save(take);
			if(result) {
				return new BaseResponse<String>(1,"提交成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<String>(0,"提交失败");
	}
	
	@RequestMapping("/takeInfo")
	@ResponseBody
    public BaseResponse<Take> takeInfo(
    		@RequestParam(value="takeId",required=false)String takeId){
		try {
			Take result = takeService.getById(takeId);
			return new BaseResponse<Take>(1,"成功",result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<Take>(0,"失败",null);
	}
	
	@RequestMapping("/takeUpdate")
	@ResponseBody
    public BaseResponse<String> takeUpdate(
    		@RequestParam(value="takeId",required=false)String takeId,
    		@RequestParam(value="content",required=false)String content){
		try {
			Take take = new Take();
			take.setTakeId(takeId);
			take.setTakeContent(content);
			boolean result = takeService.update(take);
			if(result) {
				return new BaseResponse<String>(1,"修改成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<String>(0,"修改失败");
	}
	
	@RequestMapping("/takeDel")
	@ResponseBody
    public BaseResponse<String> takeDel(
    		@RequestParam(value="takeId",required=false)String takeId){
		try {
			boolean result = takeService.remove(takeId);
			if(result) {
				return new BaseResponse<String>(1,"删除成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<String>(0,"删除失败");
	}
	
	@RequestMapping("/takeList")
	@ResponseBody
    public BaseResponse<List<Take>> takeList(
    		@RequestParam(value="accountId",required=false)String accountId){
		Take take = new Take();
		try {
			take.setTakeAccountId(KLKJUtil.isNotEmpty(accountId)?accountId:null);
			List<Take> list = takeService.getList(take);
			return new BaseResponse<List<Take>>(0,"SUCCESS",list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<List<Take>>(0,"ERROR",null);
	}
	
	@RequestMapping("/saveCar")
	@ResponseBody
	public BaseResponse<String> saveCar(
    		@RequestParam(value="productId",required=false)String productId) throws IOException{
		try {
			ShoppingCart car = ShoppingCart.getShoppingCart();
			CarObj obj = new CarObj();
			Product product = productService.getById(productId);
			obj.setId(product.getProductId());
			obj.setTitle(product.getProductTitle());
			obj.setImage(product.getProductLogo());
			obj.setPrice(product.getProductPrice());
			obj.setCount(1);
			//obj.setHot(product.getProductHot());
			boolean isSave = car.addCar(obj.getId(), obj);
			if(isSave){
				return new BaseResponse<String>(1,"SUCCESS","添加购物车成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<String>(0,"ERROR",null);
	}
	
	@RequestMapping("/showCar")
	@ResponseBody
	public Map<String,Object> showCar() throws Exception{
		List<CarObj> carObjList = new ArrayList<CarObj>();
		BigDecimal total = new BigDecimal(0);
		Map<String,Object> resMap = new HashMap<String,Object>();
		int count = 0;
		ShoppingCart car = ShoppingCart.getShoppingCart();
		for (Map.Entry<String, CarObj> entry : car.getCar().entrySet()) {
			  carObjList.add(entry.getValue());
		}
		total = car.getTotal();
		for (CarObj carObj : carObjList) {
			carObj.setTotal(new BigDecimal(carObj.getCount()).multiply(carObj.getPrice()));
			count ++;
		}
		resMap.put("carObjList", carObjList);
		resMap.put("count", count);
		resMap.put("total", total);
		return resMap;
	}
	
	@RequestMapping("/editCar")
	@ResponseBody
	public BaseResponse<String> editCar(
    		@RequestParam(value="productId",required=false)String id,
    		@RequestParam(value="count",required=false)int count) throws IOException{
		try {
			if(count<=1){
				ShoppingCart car = ShoppingCart.getShoppingCart();
				boolean isDelete = car.removeCar(id);
				if(isDelete){
					return new BaseResponse<String>(0,"ERROR","移除失败");
				}
			}else{
				count = count -1;
				ShoppingCart car = ShoppingCart.getShoppingCart();
				CarObj obj = car.getCar().get(id);
				obj.setCount(count);
				boolean isSave = car.editCar(obj.getId(), obj);
				if(isSave){
					return new BaseResponse<String>(1,"SUCCESS","操作成功");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<String>(0,"ERROR",null);
	}
	
	@RequestMapping("/saveCarOrders")
	@ResponseBody
	public BaseResponse<String> saveCarOrders(
    		@RequestParam(value="accountId",required=false)String accountId,
    		@RequestParam(value="address",required=false)String address) throws Exception {
		Orders orders = null;
		boolean flag = true;
		String ID = KLKJID.uuid();
		String productId = null;
		String itemJson = null;
		BigDecimal total = new BigDecimal(0);
		try {
			if(flag){
				List<CarObj> carObjList = new ArrayList<CarObj>();
				ShoppingCart car = ShoppingCart.getShoppingCart();
				for (Map.Entry<String, CarObj> entry : car.getCar().entrySet()) { 
				  carObjList.add(entry.getValue());
				}
				List<Item> itemList = new ArrayList<Item>();
				Item item = null;
				for (CarObj carObj : carObjList) {
					item = new Item();
					item.setProductid(carObj.getId());
					item.setOrdersid(ID);
					item.setTitle(carObj.getTitle());
					item.setCount(carObj.getCount());
					item.setTotal(carObj.getTotal());
					total = total.add(carObj.getTotal());
					itemList.add(item);
				}
				if(KLKJUtil.isNotEmpty(carObjList)){
					productId = carObjList.get(0).getId();
					itemJson = KLKJJsonUtil.toJson(itemList);
				}
				orders = new Orders();
				orders.setId(ID);
				orders.setAccountid(accountId);
				orders.setAddress(address);
				orders.setWay(itemJson);
				orders.setCount(carObjList.size());
				orders.setProductid(productId);
				orders.setState(0);
				orders.setTime(KLKJUtil.getDateTimeStr());
				orders.setDate(KLKJUtil.getDateStr());
				orders.setNumber("bs"+new Date().getTime());
				orders.setTotal(total);
				ordersService.saveOrUpdateOrders(orders);
				car.clear();
				return new BaseResponse<String>(1,"SUCCESS",ID);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<String>(0,"ERROR",null);
	}
	
	@RequestMapping("/ordersDetail")
	@ResponseBody
	public Orders ordersDetail(
    		@RequestParam(value="ordersId",required=false)String ordersId) throws Exception{
		Orders orders = null;
		List<Common> commons = new ArrayList<Common>();
		try {
			orders = ordersService.loadOrders(ordersId);
			if(KLKJUtil.isNotEmpty(orders) && KLKJUtil.isNotEmpty(orders.getWay())){
				List<Item> itemList = KLKJJsonUtil.fromJson(orders.getWay(), new TypeToken<List<Item>>() {}.getType());
				for (Item item : itemList) {
					Common common = commonService.getByOrdersId(ordersId, item.getProductid());
					if(KLKJUtil.isNotEmpty(common)) {
						item.setHasCommon(1);
						commons.add(common);
					}
				}
				orders.setItemList(itemList);
				orders.setCommons(commons);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return orders;
	}

	@RequestMapping("/updateOrdersState")
	@ResponseBody
	public BaseResponse<String> updateOrdersState(
    		@RequestParam(value="ordersId",required=false)String ordersId,
    		@RequestParam(value="state",required=false)int state) throws Exception{
		try {
			if(state>0){
				Orders orders = ordersService.loadOrders(ordersId);
				orders.setState(state);
				boolean save = ordersService.saveOrUpdateOrders(orders);
				if(save){
					return new BaseResponse<String>(1,"SUCCESS","操作成功");
				}
			}else{
				boolean save = ordersService.deleteOrders(ordersId);
				if(save){
					return new BaseResponse<String>(1,"SUCCESS","操作成功");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<String>(0,"ERROR","操作失败");
	}
	//dingdan zhihou
	@RequestMapping("/ordersList")
	@ResponseBody
	public BaseResponse<List<Orders>> ordersList(
    		@RequestParam(value="accountId",required=false)String accountId,
    		@RequestParam(value="state",required=false)int state) throws Exception{
		List<Orders> result = new ArrayList<Orders>();
		/*if(value==0){return '未付款';}
		if(value==1){return '已付款';}
		if(value==2){return '已发货';}
		if(value==3){return '已完成';}*/
		try {
			List<Orders> list = ordersService.loadOrdersListByAccountid(accountId);
			int type = state;//1,未付款，2，未完成，3，已完成
			if(type > 0){
				for (Orders orders : list) {
					if(type==1 && orders.getState() == 0){
						result.add(orders);
					}
					if(type==2 && (orders.getState() == 1 || orders.getState() == 2)){
						result.add(orders);
					}
					if(type==3 && orders.getState() == 3){
						result.add(orders);
					}
				}
			}else{
				result.addAll(list);
			}
			for (Orders orders : result) {
				if(KLKJUtil.isNotEmpty(orders.getAddress())){
					List<Item> itemList = KLKJJsonUtil.fromJson(orders.getWay(), new TypeToken<List<Item>>() {}.getType());
					orders.setItemList(itemList);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<List<Orders>>(1,"SUCCESS",result);
	}

	@RequestMapping("/deleteOrders")
	@ResponseBody
	public BaseResponse<String> deleteOrders(
    		@RequestParam(value="ordersId",required=false)String ordersId) throws Exception{
		try {
			boolean save = ordersService.deleteOrders(ordersId);
			if(save){
				return new BaseResponse<String>(1,"SUCCES","删除成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<String>(0,"ERROR","操作失败");
	}

	@RequestMapping("/productDetail")
	@ResponseBody
	public Product productDetail(
    		@RequestParam(value="productId",required=false)String productId) throws Exception{
		Map<String,Object> resMap = new HashMap<String,Object>();
		resMap.put("product", null);
		try {
			Product product = productService.getById(productId);
			Common common = new Common();
			common.setCommonProductId(productId);
			List<Common> list = commonService.getList(common);
			product.setCommentList(list);
			return product;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	@RequestMapping("/dataItem")
	@ResponseBody
	public Map<String,Object> dataItem(
    		@RequestParam(value="accountId",required=false)String accountId) throws Exception{
		Map<String,Object> resMap = new HashMap<String,Object>();
		resMap.put("zong", "0");
		resMap.put("dan", "0");
		BigDecimal zong = new BigDecimal(0);
		try {
			List<Orders> list = ordersService.loadOrdersListByAccountid(accountId);
			if(KLKJUtil.isNotEmpty(list)){
				for (Orders orders : list) {
					zong = zong.add(orders.getTotal());
				}
			}
			resMap.put("zong", zong);
			resMap.put("dan", list.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resMap;
	}
	@RequestMapping("/saveCollect")
	@ResponseBody
    public BaseResponse<String> saveCollect(
    		@RequestParam(value="accountId",required=false)String accountId,
    		@RequestParam(value="productId",required=false)String productId){
		try {
			CollectionInfo collection = new CollectionInfo();
			collection.setCollectionAccountId(accountId);
			collection.setCollectionProductId(productId);
			List<CollectionInfo> list = collectionService.getCollectionList(collection);
			if(KLKJUtil.isNotEmpty(list)) {
				boolean result = collectionService.removeCollection(list.get(0).getCollectionId());
				if(result) {
					return new BaseResponse<String>(1,"收藏取消");
				}
			}
			collection.setCollectionId(KLKJID.uuid());
			collection.setCollectionAccountId(accountId);
			collection.setCollectionProductId(productId);
			collection.setCollectionTime(KLKJUtil.getDateTimeStr());
			boolean result = collectionService.saveCollection(collection);
			if(result) {
				return new BaseResponse<String>(1,"收藏成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<String>(0,"提交失败");
	}
	@RequestMapping("/collectionList")
	@ResponseBody
    public BaseResponse<List<CollectionInfo>> collectionList(
    		@RequestParam(value="accountId",required=false)String accountId){
		CollectionInfo collection = new CollectionInfo();
		try {
			collection.setCollectionAccountId(KLKJUtil.isNotEmpty(accountId)?accountId:null);
			List<CollectionInfo> list = collectionService.getCollectionList(collection);
			return new BaseResponse<List<CollectionInfo>>(0,"SUCCESS",list);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<List<CollectionInfo>>(0,"ERROR",null);
	}
}
