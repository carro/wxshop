package com.wx.controller;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.reflect.TypeToken;
import com.wx.entity.Account;
import com.wx.entity.BaseResponse;
import com.wx.entity.Product;
import com.wx.entity.Common;
import com.wx.entity.GoodsType;
import com.wx.entity.Item;
import com.wx.entity.Orders;
import com.wx.entity.Take;
import com.wx.service.AccountService;
import com.wx.service.ProductService;
import com.wx.service.CommonService;
import com.wx.service.TypeService;
import com.wx.service.OrdersService;
import com.wx.service.TakeService;
import com.wx.utils.KLKJID;
import com.wx.utils.KLKJJsonUtil;
import com.wx.utils.KLKJUtil;


@Controller
@RequestMapping("/admin")
public class DataController {
	
	private static final Logger logger = LoggerFactory.getLogger(DataController.class);

	@Autowired
	private ProductService productService;
	@Autowired
	private TypeService typeService;
	@Autowired
	private AccountService accountService;
	@Autowired
	private CommonService commonService;
	@Autowired
	private TakeService takeService;
	@Autowired
	private OrdersService ordersService;
	
	
	@RequestMapping("/productCreate")
	public String productCreate(
    		@RequestParam(value="productId",required=false)String productId,Model model) {
		try {
			Product product = productService.getById(productId);
			List<GoodsType> typeList = typeService.getList();
			model.addAttribute("typeList", typeList);
			model.addAttribute("product", product);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "product/productCreate";
	}
	@RequestMapping("/typeCreate")
	public String typeCreate(
    		@RequestParam(value="typeId",required=false)String typeId,Model model) {
		try {
			GoodsType type = typeService.getById(typeId);
			model.addAttribute("type", type);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "product/typeCreate";
	}
	/**
	 * @Description: 订单记录管理页面
	 * @author diancan
	 */
	@RequestMapping("/ordersManage")
	public String ordersManage() throws Exception {
		return "product/ordersManage";
	}
	
	@RequestMapping("/saveProduct")
	@ResponseBody
	public BaseResponse<String> saveProduct(
			HttpServletRequest request,
    		@RequestParam(value="productId",required=false)String productId,
    		@RequestParam(value="productTypeId",required=false)String productTypeId,
    		@RequestParam(value="productTitle",required=false)String productTitle,
    		@RequestParam(value="productLogo",required=false)String productLogo,
    		@RequestParam(value="productCode",required=false)String productCode,
    		@RequestParam(value="productBrand",required=false)String productBrand,
    		@RequestParam(value="productPrice",required=false)BigDecimal productPrice,
    		@RequestParam(value="productPublish",required=false)String productPublish,
    		@RequestParam(value="productSubtitle",required=false)String productSubtitle,
    		@RequestParam(value="productFinish",required=false)String productFinish,
    		@RequestParam(value="productContent",required=false)String productContent,
    		@RequestParam(value="productTime",required=false)String productTime,
    		@RequestParam(value="productHot",required=false)Integer productHot) {
		boolean flag = false;
		Product product = new Product(productId, productTypeId, productTitle, productLogo,
				productCode, productBrand, productPrice, productPublish, productSubtitle, 
				productContent, productTime, productHot);
		try {
			if(KLKJUtil.isEmpty(productId)){
				product.setProductId(KLKJID.uuid());
				product.setProductTime(KLKJUtil.getDateTimeStr());
				flag = productService.save(product);
			}else{
				flag = productService.update(product);
			}
			if(flag){
				return new BaseResponse<String>(1,"success","操作成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<String>(0,"error","操作失败");
	}
	
	@RequestMapping("/saveType")
	@ResponseBody
	public BaseResponse<String> saveType(
			HttpServletRequest request,
    		@RequestParam(value="typeId",required=false)String typeId,
    		@RequestParam(value="typeTitle",required=false)String typeTitle) {
		boolean flag = false;
		GoodsType product = new GoodsType(typeId, typeTitle);
		try {
			if(KLKJUtil.isEmpty(typeId)){
				product.setTypeId(KLKJID.uuid());
				flag = typeService.save(product);
			}else{
				flag = typeService.update(product);
			}
			if(flag){
				return new BaseResponse<String>(1,"success","操作成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<String>(0,"error","操作失败");
	}
	
	@RequestMapping("/deleteProduct")
	@ResponseBody
	public BaseResponse<String> deleteProduct(
			HttpServletRequest request,
    		@RequestParam(value="productId",required=false)String productId) {
		boolean flag = false;
		try {
			flag = productService.remove(productId);
			if(flag){
				return new BaseResponse<String>(1,"success","操作成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<String>(0,"error","操作失败");
	}
	
	@RequestMapping("/deleteType")
	@ResponseBody
	public BaseResponse<String> deleteType(
			HttpServletRequest request,
    		@RequestParam(value="typeId",required=false)String typeId) {
		boolean flag = false;
		try {
			flag = typeService.remove(typeId);
			if(flag){
				return new BaseResponse<String>(1,"success","操作成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<String>(0,"error","操作失败");
	}
	
	@RequestMapping("/productManage")
	public String productManage(Model model) {
		try {
			List<GoodsType> typeList = typeService.getList();
			model.addAttribute("typeList", typeList);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "product/productManage";
	}
	
	@RequestMapping("/typeManage")
	public String typeManage(Model model) {
		try {
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "product/typeManage";
	}
	
	@RequestMapping("/accountManage")
	public String accountManage(Model model) {
		try {
			model.addAttribute("active", 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "product/accountManage";
	}
	
	@RequestMapping("/accountBlackManage")
	public String accountBlackManage(Model model) {
		try {
			model.addAttribute("active", 0);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "product/accountManage";
	}
	
	@RequestMapping("/commonManage")
	public String commonManage(Model model) {
		try {
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "product/commonManage";
	}
	
	@RequestMapping("/takeManage")
	public String takeManage(Model model) {
		try {
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "product/takeManage";
	}
	
	@RequestMapping("/getProduct")
	@ResponseBody
	public List<Product> getProduct(
    		@RequestParam(value="searchTypeId",required=false)String searchTypeId,
    		@RequestParam(value="searchTitle",required=false)String searchTitle,
			HttpServletRequest request) {
		List<Product> list = new ArrayList<Product>();
		Product product = new Product();
		try {
			product.setProductTypeId(KLKJUtil.isNotEmpty(searchTypeId)?searchTypeId:null);
			product.setProductTitle(KLKJUtil.isNotEmpty(searchTitle)?searchTitle:null);
			list = productService.getList(product);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping("/getType")
	@ResponseBody
	public List<GoodsType> getType() {
		List<GoodsType> list = new ArrayList<GoodsType>();
		try {
			list = typeService.getList();
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping("/accountList")
	@ResponseBody
	public List<Account> accountList(
    		@RequestParam(value="active",required=false)Integer accountActive,
			HttpServletRequest request) {
		List<Account> list = new ArrayList<Account>();
		Account account = new Account();
		try {
			account.setAccountActive(KLKJUtil.isNotEmpty(accountActive)?accountActive:null);
			list = accountService.getList(account);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping("/accountActive")
	@ResponseBody
	public BaseResponse<String> accountActive(
			HttpServletRequest request,
    		@RequestParam(value="active",required=false)Integer accountActive,
    		@RequestParam(value="accountId",required=false)String accountId) {
		boolean flag = false;
		Account account = new Account();
		try {
			account.setAccountId(accountId);
			account.setAccountActive(accountActive);
			flag = accountService.update(account);
			if(flag){
				return new BaseResponse<String>(1,"success","操作成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<String>(0,"error","操作失败");
	}
	
	@RequestMapping("/commonList")
	@ResponseBody
	public List<Common> commonList(
    		@RequestParam(value="searchTypeId",required=false)String searchTypeId,
    		@RequestParam(value="searchMajorId",required=false)String searchMajorId,
    		@RequestParam(value="searchInstructorId",required=false)String searchInstructorId,
			HttpServletRequest request) {
		List<Common> list = new ArrayList<Common>();
		Common common = new Common();
		try {
			//product.setProductTypeId(KLKJUtil.isNotEmpty(searchTypeId)?searchTypeId:null);
			//product.setProductMajorId(KLKJUtil.isNotEmpty(searchMajorId)?searchMajorId:null);
			//product.setProductInstructorId(KLKJUtil.isNotEmpty(searchInstructorId)?searchInstructorId:null);
			list = commonService.getList(common);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping("/deleteCommon")
	@ResponseBody
	public BaseResponse<String> deleteCommon(
			HttpServletRequest request,
    		@RequestParam(value="commonId",required=false)String commonId) {
		boolean flag = false;
		try {
			flag = commonService.remove(commonId);
			if(flag){
				return new BaseResponse<String>(1,"success","操作成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<String>(0,"error","操作失败");
	}
	
	@RequestMapping("/takeList")
	@ResponseBody
	public List<Take> takeList(
    		@RequestParam(value="searchTypeId",required=false)String searchTypeId,
    		@RequestParam(value="searchMajorId",required=false)String searchMajorId,
    		@RequestParam(value="searchInstructorId",required=false)String searchInstructorId,
			HttpServletRequest request) {
		List<Take> list = new ArrayList<Take>();
		Take take = new Take();
		try {
			//product.setProductTypeId(KLKJUtil.isNotEmpty(searchTypeId)?searchTypeId:null);
			//product.setProductMajorId(KLKJUtil.isNotEmpty(searchMajorId)?searchMajorId:null);
			//product.setProductInstructorId(KLKJUtil.isNotEmpty(searchInstructorId)?searchInstructorId:null);
			list = takeService.getList(take);
			return list;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping("/deleteTake")
	@ResponseBody
	public BaseResponse<String> deleteTake(
			HttpServletRequest request,
    		@RequestParam(value="takeId",required=false)String takeId) {
		boolean flag = false;
		try {
			flag = takeService.remove(takeId);
			if(flag){
				return new BaseResponse<String>(1,"success","操作成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<String>(0,"error","操作失败");
	}

	/**
	 * @Description: 订单记录表单
	 * @author diancan
	 */
	@RequestMapping("/diancanOrdersForm")
	public String diancanOrdersForm(
			@RequestParam(value="ordersId",required=false)String ordersId) throws Exception {
		try {
			Orders orders = ordersService.loadOrders(ordersId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	}

	/**
	 * @Description: 订单记录列表
	 * @author diancan
	 */

	@RequestMapping("/loadOrdersList")
	@ResponseBody
	public List<Orders> loadOrdersList() throws Exception {
		List<Orders> list = new ArrayList<Orders>();
		try {
			list = ordersService.loadOrdersList();
			for (Orders orders : list) {
				if(KLKJUtil.isNotEmpty(orders.getAddress())){
					List<Item> itemList = KLKJJsonUtil.fromJson(orders.getWay(), new TypeToken<List<Item>>() {}.getType());
					orders.setItemList(itemList);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	/**
	 * @Description: 订单记录新增修改
	 * @author diancan
	 */
	@RequestMapping("/saveOrders")
	@ResponseBody
	public BaseResponse<String> saveOrders(
			@RequestParam(value="ordersId",required=false)String ordersId,
			@RequestParam(value="address",required=false)String address) throws Exception {
		try {
			Orders info = new Orders();
			info.setId(ordersId);
			info.setAddress(address);
			boolean isUpdate = ordersService
					.saveOrUpdateOrders(info);
			if (isUpdate) {
				return new BaseResponse<String>(1, "SUCCESS","修改成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<String>(0, "ERROR","操作失败");
	}
	
	@RequestMapping("/changeOrders")
	@ResponseBody
	public BaseResponse<String> changeOrders(
			@RequestParam(value="ordersId",required=false)String ordersId,
			@RequestParam(value="state",required=false)int state) throws Exception {
		Orders info = ordersService.loadOrders(ordersId);
		try {
			if (KLKJUtil.isNotEmpty(info)) {
				info.setState(state);
				boolean isUpdate = ordersService
						.saveOrUpdateOrders(info);
				if (isUpdate) {
					return new BaseResponse<String>(1, "SUCCESS","修改成功");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<String>(0, "ERROR","操作失败");
	}

	/**
	 * @Description: 订单记录删除
	 * @author diancan
	 */
	@RequestMapping("/deleteOrders")
	@ResponseBody
	public BaseResponse<String> deleteOrders(
			@RequestParam(value="ordersId",required=false)String ordersId) throws Exception {
		try {
			boolean isDelete = ordersService
					.deleteOrders(ordersId);
			if (isDelete) {
				return new BaseResponse<String>(1, "SUCCESS","修改成功");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<String>(0, "ERROR","操作失败");
	}
}
