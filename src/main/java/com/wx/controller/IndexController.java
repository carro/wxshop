package com.wx.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.wx.entity.Admin;
import com.wx.entity.BaseResponse;
import com.wx.service.AdminService;
import com.wx.service.FileUploadService;
import com.wx.utils.KLKJID;
import com.wx.utils.KLKJUtil;
import com.wx.utils.KLKJWebSession;

@Controller
@RequestMapping("/admin")
public class IndexController {
	private static final Logger logger = LoggerFactory.getLogger(IndexController.class);
	@Autowired
	private AdminService adminInfoService;

	@Autowired
	private FileUploadService fileUploadService;

	@RequestMapping("")
	public String main(Model model) {
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:info/adminInfo";
	}

	@RequestMapping("/info/adminInfo")
	public String adminInfo(HttpServletRequest request, Model model) {
		try {
			Admin admin = adminInfoService.getAdminById(KLKJWebSession.getAdminSession(request).getAdminId());
			model.addAttribute("admin", admin);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "info/adminInfo";
	}

	@RequestMapping("/adminName")
	@ResponseBody
	public BaseResponse<String> adminName(HttpServletRequest request,
			@RequestParam(value = "nickname") String adminName) {
		try {
			Admin admin = KLKJWebSession.getAdminSession(request);
			if (KLKJUtil.isNotEmpty(admin)) {
				boolean update = adminInfoService.updateAdminName(admin.getAdminId(), adminName);
				if (update) {
					return new BaseResponse<String>(1, "success", "修改成功");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<String>(0, "error", "操作失败");
	}

	@RequestMapping("/adminPsw")
	@ResponseBody
	public BaseResponse<String> adminPsw(HttpServletRequest request,
			@RequestParam(value = "password2") String newPassword,
			@RequestParam(value = "password") String oldPassword) {
		try {
			Admin admin = KLKJWebSession.getAdminSession(request);
			if (KLKJUtil.isNotEmpty(admin)) {
				int result = adminInfoService.updateAdminPassword(admin.getAdminId(), newPassword, oldPassword);
				if (result == -1) {
					return new BaseResponse<String>(0, "warning", "参数不对");
				}
				if (result == 2) {
					return new BaseResponse<String>(0, "warning", "原密码不对");
				}
				if (result == 1) {
					return new BaseResponse<String>(1, "success", "修改成功");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new BaseResponse<String>(0, "error", "操作失败");
	}

	/*@RequestMapping("/uploadFile")
	@ResponseBody
	public Map<String,Object> uploadFile(
			@RequestParam(value="file" ,required=false)MultipartFile file,
			HttpServletRequest request , HttpServletResponse response){
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("status", false);
		map.put("msg", "上传失败");
		map.put("path", nullConfigure.getUrl());
		try {
			Map<String,Object> resultMap = fileUploadService.upload(file, request, response);
			map.putAll(resultMap);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("");
		}
		return map;
	}*/

	@RequestMapping("/decorator")
	public String decorator() {

		return "commons/template";
	}
	
}
