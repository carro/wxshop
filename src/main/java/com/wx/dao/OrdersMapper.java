package com.wx.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.wx.entity.Orders;
import com.wx.entity.OrdersTj;

@Mapper
public interface OrdersMapper {
	int deleteByPrimaryKey(String id);

	int insert(Orders record);

	int insertSelective(Orders record);

	Orders selectByPrimaryKey(String id);

	int updateByPrimaryKeySelective(Orders record);

	int updateByPrimaryKey(Orders record);

	List<Orders> selectAll(@Param("accountid")String accountid, @Param("date")String date,@Param("productid") String productid);

	List<Orders> selectGroupByFid();

	List<Orders> selectGroupByAid();

	List<OrdersTj> selectNearBy7Day();
}