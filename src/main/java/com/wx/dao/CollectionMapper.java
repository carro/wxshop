package com.wx.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.wx.entity.CollectionInfo;

@Mapper
public interface CollectionMapper {
    int deleteByPrimaryKey(String collectionId);

    int insertSelective(CollectionInfo record);

    CollectionInfo selectByPrimaryKey(String collectionId);

    int updateByPrimaryKeySelective(CollectionInfo record);

    int updateByPrimaryKey(CollectionInfo record);

	List<CollectionInfo> selectAll(CollectionInfo collection);
}