package com.wx.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.wx.entity.Product;

@Mapper
public interface ProductMapper {
    int deleteByPrimaryKey(String classesId);

    int insertSelective(Product record);

    Product selectByPrimaryKey(String classesId);

    int updateByPrimaryKeySelective(Product record);

    List<Product> selectAll(Product product);
    
    List<Product> selectHotAll();
}