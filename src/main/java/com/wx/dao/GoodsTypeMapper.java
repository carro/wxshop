package com.wx.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.wx.entity.GoodsType;

@Mapper
public interface GoodsTypeMapper {
	
    int deleteByPrimaryKey(String TypeId);

    int insert(GoodsType record);

    int insertSelective(GoodsType record);

    GoodsType selectByPrimaryKey(String TypeId);

    int updateByPrimaryKeySelective(GoodsType record);

    int updateByPrimaryKey(GoodsType record);
    
    List<GoodsType> selectAll();
}