package com.wx.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.wx.entity.Take;

@Mapper
public interface TakeMapper {
    int deleteByPrimaryKey(String takeId);

    int insert(Take record);

    int insertSelective(Take record);

    Take selectByPrimaryKey(String takeId);

    int updateByPrimaryKeySelective(Take record);

    int updateByPrimaryKey(Take record);

	List<Take> selectAll(Take take);
}