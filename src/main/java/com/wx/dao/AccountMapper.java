package com.wx.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.wx.entity.Account;

@Mapper
public interface AccountMapper {
    int deleteByPrimaryKey(String accountId);

    int insert(Account record);

    int insertSelective(Account record);

    Account selectByPrimaryKey(String accountId);

    int updateByPrimaryKeySelective(Account record);

    int updateByPrimaryKey(Account record);

	List<Account> selectAll(Account account);

	Account selectByOpenId(String openId);
}