package com.wx.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import com.wx.entity.Common;

@Mapper
public interface CommonMapper {
    int deleteByPrimaryKey(String commonId);

    int insertSelective(Common record);

    Common selectByOrdersId(@Param("ordersId") String ordersId, @Param("productId") String productId);

    int update(Common record);

	List<Common> selectAll(Common common);

    int updateReply(String commonId, String reply);
}