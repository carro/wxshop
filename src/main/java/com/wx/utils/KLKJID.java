package com.wx.utils;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>每人计数据ID生成工具类</p>
 * 
 * @author Alex
 *
 */
public class KLKJID {

	private static Logger logger = LoggerFactory.getLogger(KLKJID.class);
	
	/**
	 * <p>生成32位UUID, 去掉-符号</p>
	 * @return
	 */
	public static String uuid() {
		String uuid = uuid2();
		return uuid.replaceAll("-", "");
	}
	
	/**
	 * <p>生成36位UUID, 未去掉-符号</p>
	 * @return
	 */
	public static String uuid2() {
		return UUID.randomUUID().toString();
	}

	/**
	 * 生成业务流水号公共方法
	 * @param business
	 * @return
	 */
	public static String createBusinessSerial(String business) {
		try {
			if (KLKJUtil.isNotEmpty(business)) {
				//待实现, 根据业务情况和规则实现.
			}
		} catch (Exception e) {
			logger.error("生成业务流水错误, 错误参数: {}", business, e);
		}
		return uuid();
	}
	
	/**
	 * 
	 * <p>生成开放ID</p>
	 * 
	 * @param key
	 * @return
	 */
	public static String createOpenId(String key) {
		if (KLKJUtil.isEmpty(key)) {
			key = "klkj";
		}
		return key + uuid();
	}
}
