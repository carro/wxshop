package com.wx.utils;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.RequestContextUtils;

public class LocaleResolverSetLanguage {

	public static void setLanguage(HttpServletRequest request,HttpServletResponse respone,String language){
		try {
			LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);
			Locale local = new Locale(language);
			String[] arr = language.split("_");
			if(arr.length==1){
				local = new Locale(language);
			}
			else if(arr.length>1){
				local = new Locale(arr[0].toString(),arr[1].toString());
			}
			localeResolver.setLocale(request, respone, local);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
