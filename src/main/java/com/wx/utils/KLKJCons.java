package com.wx.utils;

public class KLKJCons {
	/**
	 * 订单订购产品提示
	 */
	public static final String PRODUCT_TITLE_CODE = "productTips";
	/**
	 * IP过滤设置
	 */
	public static final String Ip_Filter = "ipFilter";
	public static final String Ip_Filter_OPEN = "open";
	public static final String Ip_Filter_CLOSE = "close";
	/**
	 * 简体中文语言定义
	 */
	public static final String LANG_ZH_CN = "zh_CN";
	
	/**
	 * 繁体中文语言定义
	 */
	public static final String LANG_ZH_TW = "zh_TW";
	
	/**
	 * 美式英文语言定义
	 */
	public static final String LANG_EN_US = "en_US";
		
	/**
	 * 日期格式
	 */
	public static final String DATE = "yyyy-MM-dd";
	
	/**
	 * 日期时间格式
	 */
	public static final String DATETIMENotMin = "yyyy-MM-dd HH:mm";
	
	/**
	 * 日期时间格式
	 */
	public static final String DATETIME = "yyyy-MM-dd HH:mm:ss";
	
	/**
	 * 
	 * <p>KLKJ订购服务默认服务到期日期定义</p>
	 * 
	 */
	public static final String MRJ_OREDERED_DUE_DATE_DEFAULT = "2099-12-31";
	
	/**
	 * 数据记录数定义
	 */
	public static final String READER_TOTAL_PROPERTY = "total";
	
	/**
	 * 数据记录定义
	 */
	public static final String READER_ROOT_PROPERTY = "rows";
	
	/**
	 * 数据记录分页起始页
	 */
	public static final int READER_PAGE_INDEX = 0;
	
	/**
	 * 数据分页每页显示记录数
	 */
	public static final int READER_PAGE_SIZE = 20;
	
	/**
	 * 数据分页不分页设置
	 */
	public static final int READER_PAGE_NO = 0;
	
	/**
	 * 布尔值真
	 */
	public static final boolean TRUE = true;
	
	/**
	 * 布尔值假
	 */
	public static final boolean FALSE = false;
	
	/**
	 * 响应请求状态
	 */
	public static final String RESPONSE_STATUS = "status";
	
	/**
	 * 响应请求消息
	 */
	public static final String RESPONSE_MSG = "msg";
	
	/**
	 * 开放ID前缀关键词
	 */
	public static final String OPEN_ID_PREFIX_KEY = "mrj";
	
	/**
	 * 审批中, 等待审批状态
	 */
	public static final int APPROVAL_STATE_WAITING = 0;
	
	/**
	 * 审批通过
	 */
	public static final int APPROVAL_STATE_PASS = 1;
	
	/**
	 * 审批未通过
	 */
	public static final int APPROVAL_STATE_REFUSE = 2;
	
	/**
	 * 账号未激活状态
	 */
	public static final int ACCOUNT_STATE_NO = 0;	
	
	/**
	 * 账号激活状态
	 */
	public static final int ACCOUNT_STATE_YES = 1;
	
	/**
	 * 账号停用状态
	 */
	public static final int ACCOUNT_STATE_STOP = 2;
	
	/**
	 * 账号会话session名称定义
	 */
	public static final String ACCOUNT_SESSION = "SESSION_ACC";
	
	public static final String ACCOUNT_ROLR_SESSION = "roleType";
	
	/**
	 * 
	 * <p>KLKJ短信网关通道定义</p>
	 * 
	 */
	public static final class MRJ_SMS_CHANLE {
		/**
		 * 创蓝
		 */
		public static final String MRJ_SMS_PROVIDER_CHUANGLAN = "chuanglan";
		/**
		 * 高斯通
		 */
		public static final String MRJ_SMS_PROVIDER_GAUSSTEL = "gausstel";
	}
	
	/**
	 * 
	 * <p>KLKJ对象删除规则</p>
	 * 
	 */
	public static final class OBJECT_DELETE_RULE {
		/**
		 * 逻辑删除记录
		 */
		public static final String UPDATE = "update";
		/**
		 * 物理删除记录, 删除后将无法恢复, 所以如无必要请使用逻辑删除, 同时注意, 物理删除的地方都需要注释说明
		 */
		public static final String DELETE = "delete";
	}
	
}
