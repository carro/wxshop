package com.wx.utils.sitemesh;

import org.sitemesh.SiteMeshContext;
import org.sitemesh.content.ContentProperty;
import org.sitemesh.content.tagrules.TagRuleBundle;
import org.sitemesh.content.tagrules.html.ExportTagToContentRule;
import org.sitemesh.tagprocessor.State;

public class CustomTagRuleBundle implements TagRuleBundle{
	@Override
	public void install(State defaultState, ContentProperty contentProperty, SiteMeshContext siteMeshContext) {
		defaultState.addRule("subtitle", new ExportTagToContentRule(siteMeshContext, 
				contentProperty.getChild("subtitle"), false));
		defaultState.addRule("bodyhead", new ExportTagToContentRule(siteMeshContext, 
				contentProperty.getChild("bodyhead"), false));
//		defaultState.addRule("myStyle", new ExportTagToContentRule(siteMeshContext, 
//				contentProperty.getChild("myStyle"), false));
	}
	
	@Override
	public void cleanUp(State defaultState, ContentProperty contentProperty, SiteMeshContext siteMeshContext) {
		
	}
}
