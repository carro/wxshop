package com.wx.utils.sitemesh;

import javax.servlet.annotation.WebFilter;
import org.sitemesh.builder.SiteMeshFilterBuilder;
import org.sitemesh.config.ConfigurableSiteMeshFilter;
import org.springframework.context.annotation.Configuration;

@Configuration
@WebFilter(filterName="Sitemesh3Filter",urlPatterns="/*")
public class Sitemesh3Filter extends ConfigurableSiteMeshFilter {
	
	@Override
	protected void applyCustomConfiguration(SiteMeshFilterBuilder builder) {
		builder.addDecoratorPath("/admin", "/admin/decorator")
		.addDecoratorPath("/admin/**", "/admin/decorator")
			//白名单
			.addExcludedPath("/static/**")
			.addExcludedPath("/error/**")
			.addExcludedPath("/api/**")
			.addExcludedPath("/login")
			.addExcludedPath("/logout")
			.addTagRuleBundle(new CustomTagRuleBundle());
	}
}
