package com.wx.utils;

import java.util.HashMap;
import java.util.Map;


public class InternationalUtil {
	/**
	 * @Description: <p>获取语言列表，以后如果语言多了，这功能应该在数据库中实现</p>
	 * @return 
	 * List<String>   
	 * @author ChenJiaLu
	 * @date 2017-9-13
	 */
	public Map<String,Object> getLanguageList(){
		try {
			Map<String,Object> map = new HashMap<String, Object>();
			map.put("zh_CN", "中文");
			map.put("zh_TW", "繁体");
			map.put("en_US", "英文");
			map.put("en_TH", "泰国");
			return map;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static void main(String[] args) {
		
	}

	
}
