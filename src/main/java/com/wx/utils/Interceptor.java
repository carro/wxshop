package com.wx.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

/**
 * @Description: <p>检查是否登录的拦截器</p>
 * @author ChenJialu
 * @date 2017-5-2
 * @Title: Interceptor.java
 * @Package com.ckq.utils
 */
public class Interceptor extends HandlerInterceptorAdapter {
	
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		Object user = request.getSession().getAttribute(KLKJCons.ACCOUNT_SESSION);
		if (user == null) {
			String path = request.getContextPath();
			path = path+"/login";
			//System.out.println("尚未登录，调到登录页面"+path);
			response.sendRedirect(path);
			return false;
		}
		//System.out.println("登陆了允许访问");
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		//System.out.println("postHandle");
	}

	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		//System.out.println("afterCompletion");
	}
}
