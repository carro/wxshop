package com.wx.utils;

public class MyException extends RuntimeException{
	/**
	 * @Fields serialVersionUID : TODO
	 */
	private static final long serialVersionUID = 1L;
	
	public MyException() {}
    
    public MyException(String message) {
        super(message);
    }
    public void f() throws MyException {
        throw new MyException("用户名不对");
    }
    
}
