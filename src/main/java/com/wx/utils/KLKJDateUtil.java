package com.wx.utils;

import java.util.Calendar;
import java.util.Date;

public class KLKJDateUtil {
	
	public static String getNowAddAfter(String nowDate,int count){
		Calendar calendar = Calendar.getInstance();
        Date date = KLKJUtil.stringToDate(nowDate, KLKJCons.DATE, KLKJCons.DATE);
        calendar.setTime(date);
        calendar.add(Calendar.DATE, count);
        date = calendar.getTime();
        return KLKJUtil.date2String(date, KLKJCons.DATE);
	}
	
	public static String getNowAddAfterTime(String nowDate,int count){
		Calendar calendar = Calendar.getInstance();
        Date date = KLKJUtil.stringToDate(nowDate, KLKJCons.DATETIME, KLKJCons.DATETIME);
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, count);
        date = calendar.getTime();
        return KLKJUtil.date2String(date, KLKJCons.DATETIME);
	}
}
