package com.wx.utils;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;


public class KLKJJsonUtil {
	public static final String DATETIME = "yyyy-MM-dd HH:mm:ss";
	/**
	 * 数据记录定义
	 */
	public static final String READER_ROOT_PROPERTY = "rows";
	/**
	 * 数据记录数定义
	 */
	public static final String READER_TOTAL_PROPERTY = "total";
	
	private static Gson gson;
	
	static {
		GsonBuilder builder = new GsonBuilder();
		gson = builder.create();
	}
	
	/**
	 * <p>将Java对象进行JSON序列化</p>
	 * <p>
	 * 支持自定义时间类型格式
	 * </p>
	 * @param object
	 * @param dateFormat
	 * 			日期时间类型格式字符串
	 * @return
	 */
	public static final String toJson(Object object, String dateFormat) {
		String jsonString = "";
		if (KLKJUtil.isEmpty(dateFormat)) {
			dateFormat = KLKJJsonUtil.DATETIME;
		}
		GsonBuilder builder = new GsonBuilder();
//		if (MRJPublicCons.JSON_FORMAT.equals(anObject)) {
//			数据库后台定义的JSON格式匹配, 暂未实现
//		}
		builder.setDateFormat(dateFormat);
		Gson gson = builder.create();
		jsonString = gson.toJson(object);
		return jsonString;
	}
	
	/**
	 * <p>将Java对象进行Json序列化</p>
	 * @param pObject
	 * @return
	 */
	public static final String toJson(Object pObject) {
		String jsonString = toJson(pObject, KLKJJsonUtil.DATETIME);
		return jsonString;
	}
	
	/**
	 * 将Java集合对象序列化为表格分页所需的Json对象<b>(服务器端分页)</b>
	 * <p>
	 * 缺省的日期时间类型为：yyyy-MM-dd HH:mm:ss
	 * <p>
	 * 
	 * @param pList 集合对象
	 * @param total 集合总数 
	 * @return
	 */
	public static final String toGridJson(List<? extends Object> pList, long total) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(KLKJJsonUtil.READER_ROOT_PROPERTY, pList);
		map.put(KLKJJsonUtil.READER_TOTAL_PROPERTY, total);
		String jsonString = toJson(map, KLKJJsonUtil.DATETIME);
		return jsonString;
	}
	
	/**
	 * 将Java集合对象序列化为表格分页所需的Json对象<b>(前台客户端分页或不分页)</b>
	 * <p>
	 * 缺省的日期时间类型为：yyyy-MM-dd HH:mm:ss
	 * <p>
	 * 
	 * @param pList 集合对象
	 * @return
	 */
	public static final String toGridJson(List<? extends Object> pList) {
		String jsonString = toJson(pList, KLKJJsonUtil.DATETIME);
		return jsonString;
	}

	/**
	 * 将Json字符串转换为Java对象
	 * 
	 * @param json
	 * @param type
	 *            如果Java对象是一个普通类则直接JsonUtils.fromJson(json,
	 *            HashDto.class);即可。如果是一个泛型类(如一个dto集合类)则需要
	 *            使用如下方式传参：JsonUtils.fromJson(json, new
	 *            TypeToken<List<HashDto>>() {}.getType());
	 * @return
	 */
	public static final <T> T fromJson(String json, Type type) {
		T list = gson.fromJson(json, type);
		return (T) list;
	}

	/**
	 * 将json转换为List<Dto>集合对象的简便方法
	 * 
	 * @param json
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static final List fromJson(String json) {
		List list = fromJson(json, new TypeToken<List>() {}.getType());
		return list;
	}
}
