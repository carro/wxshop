package com.wx.utils.pay;

import javax.servlet.http.HttpServletRequest;

import com.wx.utils.KLKJUtil;

public class Configure {
	//小程序ID	
	private static String appID = "[Your Appid]";
	//小程序秘钥
	private static String appSecret = "[Your AppSecret]";
	//grant_type
	private static String grant_type = "authorization_code";
	
	/**
	 * IpUtils工具类方法
	 * 获取真实的ip地址
	 * @param request
	 * @return
	 */
	public static String getIpAddr(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");
	    if(KLKJUtil.isNotEmpty(ip) && !"unKnown".equalsIgnoreCase(ip)){
	         //多次反向代理后会有多个ip值，第一个ip才是真实ip
	    	int index = ip.indexOf(",");
	        if(index != -1){
	            return ip.substring(0,index);
	        }else{
	            return ip;
	        }
	    }
	    ip = request.getHeader("X-Real-IP");
	    if(KLKJUtil.isNotEmpty(ip) && !"unKnown".equalsIgnoreCase(ip)){
	       return ip;
	    }
	    return request.getRemoteAddr();
	}
	
	

	public static String getAppID() {
		return appID;
	}

	public static void setAppID(String appID) {
		Configure.appID = appID;
	}


	public static String getGrant_type() {
		return grant_type;
	}

	public static void setGrant_type(String grant_type) {
		Configure.grant_type = grant_type;
	}

	public static String getAppSecret() {
		return appSecret;
	}

	public static void setAppSecret(String appSecret) {
		Configure.appSecret = appSecret;
	}

}
