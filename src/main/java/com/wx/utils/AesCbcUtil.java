package com.wx.utils;

import org.bouncycastle.jce.provider.BouncyCastleProvider;  
import javax.crypto.BadPaddingException;  
import javax.crypto.Cipher;  
import javax.crypto.IllegalBlockSizeException;  
import javax.crypto.NoSuchPaddingException;  
import javax.crypto.spec.IvParameterSpec;  
import javax.crypto.spec.SecretKeySpec;  
import java.io.UnsupportedEncodingException;  
import java.security.*;  
import java.security.spec.InvalidParameterSpecException;
import java.util.Base64;  
  
public class AesCbcUtil {  
    static {  
        //BouncyCastle是一个开源的加解密解决方案，主页在http://www.bouncycastle.org/  
        Security.addProvider(new BouncyCastleProvider());  
    }  
  
    /** 
     * AES解密 
     * 
     * @param data           //密文，被加密的数据 
     * @param key            //秘钥 
     * @param iv             //偏移量 
     * @param encodingFormat //解密后的结果需要进行的编码 
     * @return 
     * @throws Exception 
     */  
    public static String decrypt(String encryptData, String session_key, String iv, String encodingFormat) throws Exception {  
    	return decrypt(session_key, iv, encryptData);
		/* //被加密的数据  
		byte[] dataByte = Base64.getDecoder().decode(data);// Base64.decodeBase64(data);  
		//加密秘钥  
		byte[] keyByte = Base64.getEncoder().encode(dataByte);  
		//偏移量  
		byte[] ivByte = Base64.getDecoder().decode(iv);  
		try {  
		    Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");  
		    SecretKeySpec spec = new SecretKeySpec(keyByte, "AES");  
		    AlgorithmParameters parameters = AlgorithmParameters.getInstance("AES");  
		    parameters.init(new IvParameterSpec(ivByte));  
		    cipher.init(Cipher.DECRYPT_MODE, spec, parameters);// 初始化  
		    byte[] resultByte = cipher.doFinal(dataByte);  
		    if (KLKJUtil.isNotEmpty(resultByte) && resultByte.length > 0) {  
		        String result = new String(resultByte, encodingFormat);  
		        return result;  
		    }  
		    return null;  
		} catch (NoSuchAlgorithmException e) {  
		    e.printStackTrace();  
		} catch (NoSuchPaddingException e) {  
		    e.printStackTrace();  
		} catch (InvalidParameterSpecException e) {  
		    e.printStackTrace();  
		} catch (InvalidKeyException e) {  
		    e.printStackTrace();  
		} catch (InvalidAlgorithmParameterException e) {  
		    e.printStackTrace();  
		} catch (IllegalBlockSizeException e) {  
		    e.printStackTrace();  
		} catch (BadPaddingException e) {  
		    e.printStackTrace();  
		} catch (UnsupportedEncodingException e) {  
		    e.printStackTrace();  
		}  
		return null;  */
    }  
  
    public static String decrypt(String session_key, String iv, String encryptData) {
    	   
        String decryptString = "";
        //解码经过 base64 编码的字符串    
        byte[] sessionKeyByte = Base64.getDecoder().decode(session_key);
        byte[] ivByte = Base64.getDecoder().decode(iv);
        byte[] encryptDataByte = Base64.getDecoder().decode(encryptData);
     
        try {
            Security.addProvider(new BouncyCastleProvider());    
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            //得到密钥
            Key key = new SecretKeySpec(sessionKeyByte, "AES");
            //AES 加密算法
            AlgorithmParameters algorithmParameters = AlgorithmParameters.getInstance("AES");
            algorithmParameters.init(new IvParameterSpec(ivByte));
            cipher.init(Cipher.DECRYPT_MODE, key, algorithmParameters);
            byte[] bytes = cipher.doFinal(encryptDataByte);
            decryptString = new String(bytes);
        } catch (Exception e) {
                e.printStackTrace();
        }
        return decryptString;
    }
}  
