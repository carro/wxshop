package com.wx.utils;

import javax.servlet.http.HttpServletRequest;

import com.wx.entity.Admin;






public class KLKJWebSession {
	
	/**
	 * 
	 * @Description: <p>获取当前登陆用户信息</p>
	 * @author ChenJiaLu
	 * @date 2016-11-23
	 * @param @return   
	 * @return CoreUser
	 */
	public static Admin getAdminSession(HttpServletRequest request) {
		try {
			Admin admin = (Admin) request.getSession().getAttribute(KLKJCons.ACCOUNT_SESSION);
			return admin;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
