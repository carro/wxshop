package com.wx.utils;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 每人计浏览器工具类
 * </p>
 * 
 * @description
 * @project mrj-service-interface
 * @package cn.meirenji.utils
 * @author Alex
 * @created 2016-8-19 下午2:20:58
 */
public class KLKJBrowserUtil {
	
	/**
	 * <p>
	 * 获取浏览器信息
	 * </p>
	 * @param request
	 * @return
	 */
	public static String getBrowserInfo(HttpServletRequest request) {
		String agent = request.getHeader("User-Agent").toLowerCase();
		if (agent.indexOf("msie 7") > 0) {
			return "IE7";
		} else if (agent.indexOf("msie 8") > 0) {
			return "IE8";
		} else if (agent.indexOf("msie 9") > 0) {
			return "IE9";
		} else if (agent.indexOf("msie 10") > 0) {
			return "IE10";
		} else if (agent.indexOf("msie") > 0) {
			return "IE";
		} else if (agent.indexOf("opera") > 0) {
			return "Opera";
		} else if (agent.indexOf("firefox") > 0) {
			return "Firefox";
		} else if (agent.indexOf("webkit") > 0) {
			return "WebKit";
		} else if (agent.indexOf("gecko") > 0 && agent.indexOf("rv:11") > 0) {
			return "IE11";
		} else {
			return "Others";
		}
	}

	/**
	 * 
	 * <p>获取客户端浏览器的IP地址</p>
	 * 
	 * @param request
	 * @return
	 */
	public static String getBrowserClientIp(HttpServletRequest request) {
		String ip = request.getHeader("X-Forwarded-For");
		if (KLKJUtil.isNotEmpty(ip) && !"unKown".equalsIgnoreCase(ip)) {
			int index = ip.indexOf(",");
			if (index != -1) {
				return ip.substring(0, index);
			} else {
				return ip;
			}
		}
		ip = request.getHeader("X-Real-IP");
		if (KLKJUtil.isNotEmpty(ip) && !"unKown".equalsIgnoreCase(ip)) {
			return ip;
		}
		return request.getRemoteAddr();
	}
	
}
