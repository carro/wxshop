package com.wx.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KLKJUtil {

	private static Logger logger = LoggerFactory.getLogger(KLKJUtil.class);
	
	/**
	 * 判断对象是否Empty(null或元素为0)<br>
	 * 实用于对如下对象做判断:String Collection及其子类 Map及其子类
	 * 
	 * @param pObj 待检查对象
	 * 
	 * @return boolean 返回的布尔值
	 */	
	@SuppressWarnings("rawtypes")
	public static boolean isEmpty(Object pObj) {
		if (pObj == null)
			return true;
		if (pObj == "")
			return true;
		if (pObj instanceof String) {
			if (((String) pObj).length() == 0) {
				return true;
			}
		} else if (pObj instanceof Collection) {
			if (((Collection) pObj).size() == 0) {
				return true;
			}
		} else if (pObj instanceof Map) {
			if (((Map) pObj).size() == 0) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * 判断对象是否为NotEmpty(!null或元素>0)<br>
	 * 实用于对如下对象做判断:String Collection及其子类 Map及其子类
	 * 
	 * @param pObj
	 *            待检查对象
	 * @return boolean 返回的布尔值
	 */
	@SuppressWarnings("rawtypes")
	public static boolean isNotEmpty(Object pObj) {
		if (pObj == null)
			return false;
		if (pObj == "")
			return false;
		if (pObj instanceof String) {
			if (((String) pObj).length() == 0) {
				return false;
			}
		} else if (pObj instanceof Collection) {
			if (((Collection) pObj).size() == 0) {
				return false;
			}
		} else if (pObj instanceof Map) {
			if (((Map) pObj).size() == 0) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean isEquals(Object obj1,Object obj2) {
		if(obj1.equals(obj2) || obj1==obj2){
			return true;
		}
		return false;
	}
	
	/**
	 * 
	 * 将字符串转换为字符串数组通用方法.
	 * 
	 * @param string	需要转换的字符串
	 * @param split		字符串分隔符号, 默认为逗号.
	 * @return
	 */
	public static String[] stringSplitToArray(String string, String split) {
		try {
			if (isEmpty(split)) {
				split = ",";
			}
			if (isNotEmpty(string)) {
				return string.split(split);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * 获取主机名
	 * 
	 * @return
	 */
	public static String getHostName() {
		String hostName = null;
		InetAddress inetAddress = null;
		try {
			inetAddress = InetAddress.getLocalHost();
			hostName = inetAddress.getHostName();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return hostName;
	}
	
	/**
	 * 获取主机IP
	 * 
	 * @return
	 */
	public static String getHostAddress() {
		String hostAddress = null;
		InetAddress inetAddress = null;
		try {
			inetAddress = InetAddress.getLocalHost();
			hostAddress = inetAddress.getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return hostAddress;
	}
	
	/**
	 * 产生[0-9]之间的随机数
	 * 
	 * @return
	 */
	public static int random() {
		return (int) (Math.random() * 10);
	}

	/**
	 * 产生指定范围[min-max]之间的随机数
	 * 
	 * @return
	 */
	public static long randomBetween(long min, long max) {
		return Math.round(Math.random() * (max - min) + min);
	}
	
	/**
	 * 生成字符串格式的随机数, 一般用作获取验证码使用.
	 * @param number	随机字符串类型: true 纯数字类型, false 数字加字母混合类型
	 * @param length	随机字符串长度
	 * @return string
	 */
	public static String randomStr(boolean number, int length) {
		String randomStr = "";
		String strTable = number ? "1234567890" : "1234567890abcdefghijkmnpqrstuvwxyz";
		int len = strTable.length();
		boolean done = true;
		do {
			randomStr = "";
			int count = 0;
			for (int i = 0; i < length; i++) {
				double db = Math.random() * len;
				int intR = (int) Math.floor(db);
				char c = strTable.charAt(intR);
				if (('0' < c) && (c <= '9')) {
					count++;
				}
				randomStr += strTable.charAt(intR);
			}
			if (count >= 2) {
				done = false;
			}
		} while (done);
		return randomStr;
	}
	
	/**
	 * 替换空字符串，原生trim只能替换字符串前后
	 * 
	 * @param aString
	 * @return
	 */
	public static String trimAll(String aString) {
		if (KLKJUtil.isEmpty(aString)) {
			return aString;
		}
		return aString.replaceAll(" ", "");
	}
	

	/**
	 * 检查当前ClassLoader种,是否存在指定class
	 * 
	 * @param pClass
	 *            类路径
	 * @return
	 */
	public static boolean isExistClass(String pClass) {
		try {
			Class.forName(pClass);
		} catch (ClassNotFoundException e) {
			return false;
		}
		return true;
	}
	
	/**
	 * 将字符串型日期转换为日期型
	 * 
	 * @param strDate
	 *            字符串型日期
	 * @param srcDateFormat
	 *            源日期格式
	 * @param dstDateFormat
	 *            目标日期格式
	 * @return Date 返回的util.Date型日期
	 */
	public static Date stringToDate(String strDate, String srcDateFormat, String dstDateFormat) {
		Date rtDate = null;
		Date tmpDate = (new SimpleDateFormat(srcDateFormat)).parse(strDate, new ParsePosition(0));
		String tmpString = null;
		if (tmpDate != null)
			tmpString = (new SimpleDateFormat(dstDateFormat)).format(tmpDate);
		if (tmpString != null)
			rtDate = (new SimpleDateFormat(dstDateFormat)).parse(tmpString, new ParsePosition(0));
		return rtDate;
	}
	
	/**
	 * 返回指定格式的字符型日期
	 * 
	 * @param date
	 * @param formatString
	 * @return
	 */
	public static String date2String(Date date, String formatString) {
		if (isEmpty(date)) {
			return null;
		}
		SimpleDateFormat simpledateformat = new SimpleDateFormat(formatString);
		String strDate = simpledateformat.format(date);
		return strDate;
	}
	
	
	/**
	 * 返回当前日期字符串 缺省格式：yyyy-MM-dd
	 * 
	 * @return String
	 */
	public static String getDateStr() {
		return getDateStr(KLKJCons.DATE);
	}

	
	/**
	 * 返回自定义格式的当前日期时间字符串
	 * 
	 * @param format
	 *            格式规则
	 * @return String 返回当前字符串型日期时间
	 */
	public static String getDateStr(String format) {
		String returnStr = null;
		SimpleDateFormat f = new SimpleDateFormat(format);
		Date date = new Date();
		returnStr = f.format(date);
		return returnStr;
	}
	
	/**
	 * 返回指定格式的当前日期时间字符串
	 * 
	 * @param format
	 * @return
	 */
	public static String getDateTimeStr(String format) {
		String returnStr = null;
		SimpleDateFormat f = new SimpleDateFormat(format);
		Date date = new Date();
		returnStr = f.format(date);
		return returnStr;
	}

	/**
	 * 返回缺省格式的当前日期时间字符串 默认格式:yyyy-mm-dd hh:mm:ss
	 * 
	 * @return String
	 */
	public static String getDateTimeStr() {
		return getDateTimeStr(KLKJCons.DATETIME);
	}
	
	/**
	 * 比较两个时间之间间隔的秒数, 如果为空则返回0.
	 * @param dateStr	开始时间, 格式必须为yyyy-MM-dd HH:mm:ss
	 * @param dateStr1	结束时间, 格式必须为yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static int compareSecondBetweenDate(String dateStr, String dateStr1) {
		try {
			if (KLKJUtil.isEmpty(dateStr1) || KLKJUtil.isEmpty(dateStr)) {
				return 0;
			}
			Date dt1 = KLKJUtil.stringToDate(dateStr, KLKJCons.DATETIME, KLKJCons.DATETIME);
			Date dt2 = KLKJUtil.stringToDate(dateStr1, KLKJCons.DATETIME, KLKJCons.DATETIME);
			int result = (int) (dt2.getTime() - dt1.getTime())/1000;
			return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public static void main(String[] args) {
		System.out.println(KLKJUtil.randomStr(true, 16));;
	}
	
}
