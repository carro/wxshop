package com.wx.utils;

import java.util.ArrayList;
import java.util.List;

public class ChinaCityInfoUtil {

	public static void main(String[] args) {
		System.out.println(checkIncludeCity("台湾省"));
	}
	
	public static boolean checkIncludeCity(String cityString){
		List<String> list = getCityList();
		for (String string : list) {
			if(cityString==string || cityString.equals(string)){
				return true;
			}
		}
		return false;
	}
	
	public static List<String> getCityList(){
		List<String> list = new ArrayList<String>();
		list.add("北京市");
		list.add("天津市");
		list.add("上海市");
		list.add("重庆市");
		list.add("深圳市");
		list.add("安徽省");
		list.add("福建省");
		list.add("甘肃省");
		list.add("广东省");
		list.add("广西省");
		list.add("贵州省");
		list.add("海南省");
		list.add("河北省");
		list.add("河南省");
		list.add("黑龙江省");
		list.add("湖北省");
		list.add("湖南省");
		list.add("山西省");
		list.add("吉林省");
		list.add("江苏省");
		list.add("江西省");
		list.add("辽宁省");
		list.add("内蒙古");
		list.add("宁夏自治区");
		list.add("青海省");
		list.add("山东省");
		list.add("陕西省");
		list.add("四川省");
		list.add("西藏自治区");
		list.add("新疆自治区");
		list.add("云南省");
		list.add("浙江省");
		//list.add("台湾省");
		//list.add("香港行政区");
		//list.add("澳门行政区");
		return list;
	}
}
