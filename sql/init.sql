/*
SQLyog Ultimate v12.14 (64 bit)
MySQL - 5.7.36 : Database - wxshop
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`wxshop` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `wxshop`;

/*Table structure for table `account` */

DROP TABLE IF EXISTS `account`;

CREATE TABLE `account` (
  `account_id` varchar(32) NOT NULL COMMENT '主键',
  `account_nick` varchar(50) NOT NULL COMMENT '昵称',
  `account_logo` varchar(300) NOT NULL COMMENT '头像',
  `account_sex` tinyint(1) DEFAULT '0' COMMENT '性别',
  `account_openid` varchar(64) NOT NULL COMMENT 'openid',
  `account_active` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1可用，0禁用',
  `account_time` varchar(20) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户信息';

/*Data for the table `account` */

insert  into `account`(`account_id`,`account_nick`,`account_logo`,`account_sex`,`account_openid`,`account_active`,`account_time`) values 
('2425e85a411e4355984f7e0b59cc4bd8','须臾','https://thirdwx.qlogo.cn/mmopen/vi_32/L88JW7squVJaolSZgIjhNe1o0Amic0yZLITicEfunAvNanibVezFClRnqE7h13o9Vxm4dQRTrh4om8utgjbFV2zKA/132',0,'oexy959DtjGd5yDxB8Fbd6p1hHy0',1,'2024-01-26 16:57:23'),
('9fb4dc7967164159a307f5e3e674650a','须臾','https://thirdwx.qlogo.cn/mmopen/vi_32/A36iaC9JcXMpDebG81biaiaGia43DPNpSVwTTrNODA8LNaKIicJBefX3bqIuno6khS1bV9x8ibXeQPjFvGhjnTMFq8nQ/132',1,'oSqHx5LDJ30ic9dEy3aXRtb5Xl7o',0,'2021-03-16 23:54:28');

/*Table structure for table `admin` */

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `admin_id` varchar(32) NOT NULL COMMENT '主键',
  `admin_name` varchar(20) NOT NULL COMMENT '登录名',
  `admin_nickname` varchar(20) NOT NULL COMMENT '昵称',
  `admin_password` varchar(40) NOT NULL COMMENT '密码',
  `admin_login_time` datetime NOT NULL COMMENT '最后登录时间',
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='管理员信息';

/*Data for the table `admin` */

insert  into `admin`(`admin_id`,`admin_name`,`admin_nickname`,`admin_password`,`admin_login_time`) values 
('666666','admin','管理员2','e10adc3949ba59abbe56e057f20f883e','2018-09-01 15:19:57');

/*Table structure for table `collection` */

DROP TABLE IF EXISTS `collection`;

CREATE TABLE `collection` (
  `collection_id` varchar(32) NOT NULL COMMENT '主键',
  `collection_account_id` varchar(32) NOT NULL COMMENT '收藏人',
  `collection_product_id` varchar(32) NOT NULL COMMENT '收藏对象',
  `collection_time` varchar(20) NOT NULL COMMENT '时间',
  PRIMARY KEY (`collection_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='收藏';

/*Data for the table `collection` */

insert  into `collection`(`collection_id`,`collection_account_id`,`collection_product_id`,`collection_time`) values 
('550ed2b69cb54cedb8bafc7c29841c8a','2425e85a411e4355984f7e0b59cc4bd8','640a920fc19d49c8a0213ca20ccd07f4','2024-01-31 20:48:38');

/*Table structure for table `common` */

DROP TABLE IF EXISTS `common`;

CREATE TABLE `common` (
  `common_id` varchar(32) NOT NULL COMMENT '主键',
  `common_account_id` varchar(32) NOT NULL COMMENT '评论人',
  `common_orders_id` varchar(32) NOT NULL COMMENT '所属订单',
  `common_product_id` varchar(32) NOT NULL COMMENT '所属产品',
  `common_content` varchar(50) NOT NULL COMMENT '评论内容',
  `common_reply` varchar(50) DEFAULT NULL COMMENT '评论回复',
  `common_time` varchar(20) NOT NULL COMMENT '评论时间',
  PRIMARY KEY (`common_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='评论';

/*Data for the table `common` */

insert  into `common`(`common_id`,`common_account_id`,`common_orders_id`,`common_product_id`,`common_content`,`common_reply`,`common_time`) values 
('9343b7993d564642a1fee9c74fde35af','2425e85a411e4355984f7e0b59cc4bd8','2de47cd70853433a98577408b3e39c91','50475088a2124de780593168f86bb947','丝袜不错',NULL,'2024-02-02 08:53:45'),
('e94e16bdb42142abb92144273b5fe388','2425e85a411e4355984f7e0b59cc4bd8','680f42f0bef54a1f9f3e058dc448006b','ca42fea64bc14bae96b67cdbd7eb1719','顺丰水电费ssss',NULL,'2024-02-02 09:02:26'),
('f0afc3bc70454367a01f36cb604531c6','2425e85a411e4355984f7e0b59cc4bd8','680f42f0bef54a1f9f3e058dc448006b','1d74145168914443b534298b8f794c74','asfsfs锁住水分都是',NULL,'2024-02-02 09:02:18'),
('fd3510c0999343209c6ab7f087160c87','2425e85a411e4355984f7e0b59cc4bd8','2de47cd70853433a98577408b3e39c91','640a920fc19d49c8a0213ca20ccd07f4','正宗正宗正宗正宗正宗',NULL,'2024-02-02 08:54:49');

/*Table structure for table `goods_type` */

DROP TABLE IF EXISTS `goods_type`;

CREATE TABLE `goods_type` (
  `type_id` varchar(32) NOT NULL COMMENT '主键',
  `type_title` varchar(20) NOT NULL COMMENT '类别名称',
  `type_time` varchar(20) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='类别信息表';

/*Data for the table `goods_type` */

insert  into `goods_type`(`type_id`,`type_title`,`type_time`) values 
('0f9898c9631d4159ba9498c811c85e91','果鲜','2024-01-29 16:22:20'),
('461658866c93406a8af4ed2e5e20bc1f','连裤袜','2024-01-29 16:15:28'),
('8353f9ad7fcc468f8551fd9cefbc36c0','外套','2024-01-29 16:19:07'),
('a9992a71455c47c2a02ab1392ea70633','床上用品','2024-01-29 16:20:53'),
('e98e88c9c4b24f3ab4d600f7a29c519c','灯具','2024-01-29 16:26:28');

/*Table structure for table `orders` */

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` varchar(32) NOT NULL COMMENT '主键',
  `productid` varchar(32) DEFAULT NULL COMMENT '产品',
  `number` varchar(255) DEFAULT NULL COMMENT '订单编号',
  `count` int(5) DEFAULT NULL COMMENT '数量',
  `total` decimal(19,2) DEFAULT NULL COMMENT '总价',
  `address` varchar(100) DEFAULT NULL COMMENT '收货地址',
  `way` varchar(500) DEFAULT NULL COMMENT 'items',
  `state` int(1) DEFAULT NULL COMMENT '状态',
  `accountid` varchar(32) DEFAULT NULL COMMENT '下单人',
  `date` varchar(20) DEFAULT NULL COMMENT '下单日期',
  `time` varchar(20) DEFAULT NULL COMMENT '下单时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单记录';

/*Data for the table `orders` */

insert  into `orders`(`id`,`productid`,`number`,`count`,`total`,`address`,`way`,`state`,`accountid`,`date`,`time`) values 
('2de47cd70853433a98577408b3e39c91','50475088a2124de780593168f86bb947','bs1706517480789',2,'556.00','广东省深圳市福田区12345','[{\"productid\":\"50475088a2124de780593168f86bb947\",\"ordersid\":\"2de47cd70853433a98577408b3e39c91\",\"title\":\"丝袜女春秋薄\",\"count\":1,\"total\":298.00},{\"productid\":\"640a920fc19d49c8a0213ca20ccd07f4\",\"ordersid\":\"2de47cd70853433a98577408b3e39c91\",\"title\":\"车厘子大樱桃\",\"count\":2,\"total\":258.00}]',3,'2425e85a411e4355984f7e0b59cc4bd8','2024-01-29','2024-01-29 16:38:00'),
('680f42f0bef54a1f9f3e058dc448006b','ca42fea64bc14bae96b67cdbd7eb1719','bs1706703829106',2,'2275.80','好的','[{\"productid\":\"ca42fea64bc14bae96b67cdbd7eb1719\",\"ordersid\":\"680f42f0bef54a1f9f3e058dc448006b\",\"title\":\"全铜北欧满天星魔豆吸顶灯\",\"count\":1,\"total\":590.00},{\"productid\":\"1d74145168914443b534298b8f794c74\",\"ordersid\":\"680f42f0bef54a1f9f3e058dc448006b\",\"title\":\"科技布沙发床\",\"count\":1,\"total\":1685.80}]',3,'2425e85a411e4355984f7e0b59cc4bd8','2024-01-31','2024-01-31 20:23:49'),
('76cbec2cf8da425ea0f4c280b24da09e','50475088a2124de780593168f86bb947','bs1706703913386',1,'2384.00','丝袜','[{\"productid\":\"50475088a2124de780593168f86bb947\",\"ordersid\":\"76cbec2cf8da425ea0f4c280b24da09e\",\"title\":\"丝袜女春秋薄\",\"count\":8,\"total\":2384.00}]',3,'2425e85a411e4355984f7e0b59cc4bd8','2024-01-31','2024-01-31 20:25:13'),
('b0f882a159c34c76a27c39df7859da25','1d74145168914443b534298b8f794c74','bs1706749191457',1,'1685.80','哈哈哈','[{\"productid\":\"1d74145168914443b534298b8f794c74\",\"ordersid\":\"b0f882a159c34c76a27c39df7859da25\",\"title\":\"科技布沙发床\",\"count\":1,\"total\":1685.80}]',2,'2425e85a411e4355984f7e0b59cc4bd8','2024-02-01','2024-02-01 08:59:51');

/*Table structure for table `product` */

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `product_id` varchar(32) NOT NULL COMMENT '主键',
  `product_type_id` varchar(32) NOT NULL COMMENT '所属类别',
  `product_title` varchar(50) NOT NULL COMMENT '产品名称',
  `product_logo` varchar(300) NOT NULL COMMENT '封面',
  `product_code` varchar(50) DEFAULT NULL COMMENT '编号',
  `product_brand` varchar(30) NOT NULL COMMENT '品牌',
  `product_price` decimal(16,2) NOT NULL COMMENT '单价',
  `product_publish` varchar(10) DEFAULT NULL COMMENT '上市时间',
  `product_subtitle` varchar(200) DEFAULT NULL COMMENT '规格',
  `product_content` varchar(3000) DEFAULT NULL COMMENT '内容',
  `product_time` varchar(20) DEFAULT NULL COMMENT '创建时间',
  `product_hot` tinyint(1) DEFAULT '0' COMMENT '1热门推荐',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜色信息';

/*Data for the table `product` */

insert  into `product`(`product_id`,`product_type_id`,`product_title`,`product_logo`,`product_code`,`product_brand`,`product_price`,`product_publish`,`product_subtitle`,`product_content`,`product_time`,`product_hot`) values 
('1d74145168914443b534298b8f794c74','a9992a71455c47c2a02ab1392ea70633','科技布沙发床','/file/download/fc2b8b67-2295-46c9-ac54-0d6526a4f4bb/inline/png','10091668145670','霆估小店','1685.80','2023-10-04','新中式','商品名称：科技布沙发床两用现代简约单双人客厅小户型多功能储物收纳可折叠 1.15米高回弹海绵 颜色备注 1.5米-1.8米商品编号：10091668145670店铺： 霆估小店商品毛重：1.0kg面料材质：皮毛一体风格：新中式','2024-01-29 16:21:44',1),
('50475088a2124de780593168f86bb947','461658866c93406a8af4ed2e5e20bc1f','丝袜女春秋薄','/file/download/d5993359-c3bb-4a10-81ac-3753f4bb4722/inline/png','10088651669382',' CRANTA KOMEIA','298.00','2024-01-29','普通裆','品牌： CRANTA KOMEIA\r\n商品名称：CRANTA KOMEIA丝袜女春秋薄防勾丝性感光腿神器打底裤袜生日新年礼物送女生礼盒 咖透肤连脚 高级感轻奢送女朋友实用老婆商品编号：10088651669382店铺： CRANTA KOMEIA配饰旗舰店商品毛重：1.0kg商品产地：中国大陆货号：kf5518a厚度：薄款材质：涤纶(聚酯纤维)54.2%类别：连裤袜图案：纯色丹尼数：40-80D适用季节：秋季，春季裆位款式：普通裆功能：保暖，美腿塑形','2024-01-29 16:17:08',1),
('640a920fc19d49c8a0213ca20ccd07f4','0f9898c9631d4159ba9498c811c85e91','车厘子大樱桃','/file/download/de8f6c44-79ee-4fb2-8cf9-4581c16f11b9/inline/png','20180186520','同城果鲜','129.00','2024-01-29','进口','品牌： 同城果鲜（tongchengguoxian）\r\n商品名称：同城果鲜（tongchengguoxian）进口车厘子大樱桃2斤JJ级大果 生鲜 孕妇时令新鲜水果商品编号：20180186520店铺： 同城果鲜生鲜店商品毛重：1.0kg商品产地：智利国产/进口：进口贮存条件：冷藏原产地：智利','2024-01-29 16:23:52',0),
('96b0e44cdabb482ca1958fa2e5177e39','8353f9ad7fcc468f8551fd9cefbc36c0','棉衣男两面穿冬季棉服','/file/download/09e78e98-94ff-44d5-b268-78d020105c09/inline/png','10060639159743','贵人鸟','149.90','2024-01-01','简约风','品牌： 贵人鸟\r\n商品名称：贵人鸟 棉衣男两面穿冬季棉服纯色治愈色外套男士棉袄立领面包服情侣款 卡其 M商品编号：10060639159743店铺： 贵人鸟男装旗舰店商品毛重：500.00g商品产地：中国大陆货号：GB52M2218Y01A材质：涤纶(聚酯纤维)100%版型：宽松型厚度：加厚衣门襟：拉链衣长：常规款领型：立领穿着方式：两面穿流行元素：印花，简约适用人群：青年填充物：涤纶(聚酯纤维)上市时间：2022年冬季图案：纯色，字母，LOGO适用场景：居家，学校风格：休闲风休闲风：简约风','2024-01-29 16:19:53',1),
('acaa37613c0f408ab17dc91a8a617192','0f9898c9631d4159ba9498c811c85e91','红心猕猴桃奇异果','/file/download/2176f75b-40d8-46a2-9de7-a44bd0e4d070/inline/png','10062421769105','同城果鲜','55.90','2024-01-28','箱','品牌： 同城果鲜（tongchengguoxian）\r\n商品名称：同城果鲜（tongchengguoxian）同城 红心猕猴桃奇异果新鲜水果时令孕妇弥猴桃 大果 约30个 90g商品编号：10062421769105店铺： 同城果鲜生鲜店商品毛重：500.00g品种：红阳贮存条件：冷藏国产/进口：国产','2024-01-29 16:25:45',1),
('ca42fea64bc14bae96b67cdbd7eb1719','e98e88c9c4b24f3ab4d600f7a29c519c','全铜北欧满天星魔豆吸顶灯','/file/download/5a6c91f8-1f77-4639-bb09-ec6815cf9a31/inline/png','10045174514235','帕莎','590.00','2023-08-05','套','品牌： 帕莎\r\n商品名称：帕莎 全铜北欧满天星魔豆吸顶灯创意网红轻奢客厅灯现代简约卧室灯 8+1-铜本色-配三色光源商品编号：10045174514235店铺： 帕莎官方旗舰店商品毛重：4.9kg商品产地：中国大陆货号：8377操控方式：开关式最大瓦数：50-100W(含)表面处理：拉丝垂吊最大照射面积：20-30㎡(含)色温：三色可调灯身材质：铜风格：轻奢风光源类型：LED灯罩材质：玻璃适用场景：客厅，卧室，餐厅','2024-01-29 16:27:29',1);

/*Table structure for table `take` */

DROP TABLE IF EXISTS `take`;

CREATE TABLE `take` (
  `take_id` varchar(32) NOT NULL,
  `take_account_id` varchar(32) NOT NULL,
  `take_content` varchar(200) NOT NULL,
  `take_time` varchar(20) NOT NULL,
  PRIMARY KEY (`take_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='留言';

/*Data for the table `take` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
